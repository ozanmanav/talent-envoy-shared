export const formatObjectCandidateAnswers = remoteObject => {
  let candidateAnswersObject = {};

  for (const candidateAnswer in remoteObject) {
    let question = remoteObject[candidateAnswer]?.question;
    let answerValue = remoteObject[candidateAnswer].value;
    candidateAnswersObject[`${question.name}`] = answerValue;
  }

  return candidateAnswersObject;
};
