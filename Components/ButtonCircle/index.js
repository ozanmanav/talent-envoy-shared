import React from "react";
import { TouchableOpacity, View, StyleSheet } from "react-native";
import { CustomText, Icon } from "../";
import { Icons, Colors } from "@Theme";

// type is primary, success or simple

const ButtonCircle = ({ type = "simple", label = [], icon, onPress }) => {
  const iconContainerStyle = styles[type + "Container"];
  const tintColor = type === "simple" ? Colors.DARKEST : Colors.WHITE;

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <View style={iconContainerStyle}>
          <Icon
            iconSource={Icons[icon]}
            iconWidth={22}
            iconHeight={22}
            tintColor={tintColor}
          />
        </View>
        {label.map(l => (
          <CustomText key={l} style={styles.label}>
            {l}
          </CustomText>
        ))}
      </View>
    </TouchableOpacity>
  );
};

export default ButtonCircle;

const common = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  width: 72,
  height: 72,
  marginBottom: 8,
  borderRadius: 36
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center"
  },
  simpleContainer: {
    ...common,
    backgroundColor: Colors.WHITE,
    borderWidth: 1,
    borderColor: Colors.GREY_LIGHTEST
  },
  primaryContainer: {
    ...common,
    backgroundColor: Colors.PRIMARY
  },
  successContainer: {
    ...common,
    backgroundColor: Colors.SUCCESS
  },
  label: {
    color: Colors.DARK
  }
});
