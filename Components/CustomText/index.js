import React from "react";
import { Text, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import { Fonts } from "@Theme";

const CustomText = props => {
  let { type, style } = props;
  let fontFamily = type ? Fonts.family[type] : Fonts.family.medium;

  return (
    <Text {...props} style={[styles.text, style, { fontFamily }]}>
      {props.children}
    </Text>
  );
};

CustomText.propTypes = {
  children: PropTypes.node,
  type: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object])
};

export default CustomText;

const styles = StyleSheet.create({
  text: {
    fontWeight: "300"
  }
});
