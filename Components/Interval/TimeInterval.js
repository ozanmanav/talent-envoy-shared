import { Icon } from "@shared/Components";
import { Colors, Icons } from "@Theme";
import { equals, findIndex, split } from "ramda";
import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { compose, withHandlers, withProps } from "recompose";
import theme from "./theme";
import timeData from "./timeData";
import WheelPicker from "./WheelPicker";

const TimeInterval = ({
  indexes,
  startHourChanged,
  startMinuteChanged,
  endHourChanged,
  endMinuteChanged,
  endChanged,
  deletable,
  _onDelete,
  disabled,
  interval
}) => {
  return (
    <>
      <View style={styles.interval}>
        <View style={styles.time}>
          <WheelPicker
            initialScrollIndex={indexes.startHour - 1}
            data={timeData.hours}
            onChange={startHourChanged}
            width={30}
          />
          <Text style={styles.column}>:</Text>
          <WheelPicker
            initialScrollIndex={indexes.startMinute - 1}
            data={timeData.minutes}
            onChange={startMinuteChanged}
            width={35}
          />
        </View>

        <Icon iconSource={Icons.arrowRight} iconHeight={20} iconWidth={20} />

        <View style={styles.time}>
          <WheelPicker
            initialScrollIndex={indexes.endHour - 1}
            data={timeData.hours}
            onChange={endHourChanged}
            width={30}
          />
          <Text style={styles.column}>:</Text>
          <WheelPicker
            initialScrollIndex={indexes.endMinute - 1}
            data={timeData.minutes}
            onChange={endMinuteChanged}
            width={35}
          />
        </View>

        {deletable && !disabled && (
          <TouchableOpacity
            style={styles.deleteIconContainer}
            onPress={_onDelete}
          >
            <Image
              style={styles.deleteIcon}
              source={Icons.failed}
              resizeMode="contain"
            />
          </TouchableOpacity>
        )}
        {disabled && <View style={styles.dimmed} />}
      </View>
    </>
  );
};

const indexOfStr = data => str => findIndex(equals(str), data);
const indexOfHour = indexOfStr(timeData.hours);
const indexOfMinute = indexOfStr(timeData.minutes);
const splitByColumn = split(":");

export default compose(
  withProps(({ interval }) => {
    const [startHour, startMinute] = splitByColumn(interval.start);
    const [endHour, endMinute] = splitByColumn(interval.end);

    return {
      indexes: {
        startHour: indexOfHour(startHour),
        startMinute: indexOfMinute(startMinute),
        endHour: indexOfHour(endHour),
        endMinute: indexOfMinute(endMinute)
      }
    };
  }),
  withHandlers({
    startHourChanged: props => startHourIndex => {
      const { interval, indexes, onIntervalChange } = props;
      const hour = timeData.hours[startHourIndex];
      const minute = timeData.minutes[indexes.startMinute];
      const start = hour + ":" + minute;
      onIntervalChange({ ...interval, start });
    },
    startMinuteChanged: props => startMinuteIndex => {
      const { interval, indexes, onIntervalChange } = props;
      const hour = timeData.hours[indexes.startHour];
      const minute = timeData.minutes[startMinuteIndex];
      const start = hour + ":" + minute;
      onIntervalChange({ ...interval, start });
    },
    endHourChanged: props => endHourIndex => {
      const { interval, indexes, onIntervalChange } = props;
      const hour = timeData.hours[endHourIndex];
      const minute = timeData.minutes[indexes.endMinute];
      const end = hour + ":" + minute;
      onIntervalChange({ ...interval, end });
    },
    endMinuteChanged: props => endMinuteIndex => {
      const { interval, indexes, onIntervalChange } = props;
      const hour = timeData.hours[indexes.endHour];
      const minute = timeData.minutes[endMinuteIndex];
      const end = hour + ":" + minute;
      onIntervalChange({ ...interval, end });
    },
    _onDelete: ({ onDelete, interval }) => () => onDelete(interval)
  })
)(TimeInterval);

const styles = StyleSheet.create({
  interval: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    borderWidth: theme.INTERVAL.borderWidth,
    borderRadius: theme.INTERVAL.borderRadius,
    borderColor: theme.INTERVAL.borderColor,
    marginBottom: theme.INTERVAL.marginBottom,
    paddingHorizontal: 10
  },
  time: {
    display: "flex",
    flexDirection: "row"
  },
  column: {
    fontSize: 24,
    alignSelf: "center",
    marginHorizontal: 5
  },
  deleteIconContainer: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    top: theme.DELETE_CONTAINER.top,
    right: theme.DELETE_CONTAINER.right,
    width: theme.DELETE_CONTAINER.width,
    height: theme.DELETE_CONTAINER.height,
    borderRadius: theme.DELETE_CONTAINER.borderRadius,
    borderWidth: theme.DELETE_CONTAINER.borderWidth,
    borderColor: theme.DELETE_CONTAINER.borderColor,
    backgroundColor: theme.DELETE_CONTAINER.backgroundColor
  },
  deleteIcon: {
    width: theme.DELETE_ICON.width,
    height: theme.DELETE_ICON.height,
    tintColor: theme.DELETE_ICON.tintColor
  },
  dimmed: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: Colors.GREY,
    opacity: 0.5
  }
});
