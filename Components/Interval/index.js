import Time from "./TimeInterval";
import Date from "./DateInterval";
import DayTime from "./DayTime";

export default {
  Time,
  Date,
  DayTime
};
