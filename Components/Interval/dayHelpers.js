import moment from "moment";

// 2019 Jan Wed 29

const getDays = interval => {
  const intervalStartDay = moment(interval.startDay, "YYYY MMM ddd DD");
  const intervalEndDay = moment(interval.endDay, "YYYY MMM ddd DD");

  const now = moment();
  const startDay =
    intervalStartDay.isValid() && intervalStartDay.isBefore(now)
      ? intervalStartDay
      : now;
  const temp = moment(startDay);
  const days = [" "];
  const endDay = moment(now).add(14, "days");
  while (temp.isBefore(endDay)) {
    if (temp.date() === now.date()) days.push("Today");
    else days.push(temp.format("MMM ddd DD"));
    temp.add(1, "days");
  }
  days.push("  ");

  const startDayIndex = intervalStartDay.isValid()
    ? intervalStartDay.diff(startDay, "days")
    : 1;
  const endDayIndex = intervalEndDay.isValid()
    ? intervalEndDay.diff(startDay, "days")
    : 1;

  const todayIndex = now.diff(startDay, "days") + 1;

  return { days, todayIndex, firstDate: startDay, startDayIndex, endDayIndex };
};

const getDaysForDayTime = dayTime => {
  const dayTimeStartDay = moment(dayTime.day, "YYYY MMM ddd DD");

  const now = moment();
  const startDay =
    dayTimeStartDay.isValid() && dayTimeStartDay.isBefore(now)
      ? dayTimeStartDay
      : now;
  const temp = moment(startDay);
  const days = [" "];
  const endDay = moment(now).add(14, "days");
  while (temp.isBefore(endDay)) {
    if (temp.date() === now.date()) days.push("Today");
    else days.push(temp.format("MMM ddd DD"));
    temp.add(1, "days");
  }
  days.push("  ");

  const dayIndex = dayTimeStartDay.isValid()
    ? dayTimeStartDay.diff(startDay, "days")
    : 1;

  const todayIndex = now.diff(startDay, "days") + 1;

  return { days, todayIndex, firstDate: startDay, dayIndex };
};

const indexToDay = (index, initialDate) => {
  const day = moment(initialDate).add(index - 1, "days");
  const dayWithFormat = day.format("YYYY MMM ddd DD");
  return dayWithFormat;
};

export default {
  getDays,
  getDaysForDayTime,
  indexToDay
};
