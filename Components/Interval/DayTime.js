import { Colors, Icons } from "@Theme";
import { equals, findIndex, split } from "ramda";
import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { compose, withHandlers, withProps, withStateHandlers } from "recompose";
import dayHelpers from "./dayHelpers";
import theme from "./theme";
import timeData from "./timeData";
import WheelPicker from "./WheelPicker";

const DayTime = ({
  indexes,
  days,
  firstDate,
  hourChanged,
  minuteChanged,
  dayChanged,
  deletable,
  _onDelete
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.wheels}>
        <WheelPicker
          initialScrollIndex={indexes.day}
          data={days}
          onChange={dayChanged}
          width={145}
        />
        <View style={styles.time}>
          <WheelPicker
            initialScrollIndex={indexes.hour - 1}
            data={timeData.hours}
            onChange={hourChanged}
            width={35}
          />
          <Text style={styles.column}>:</Text>
          <WheelPicker
            initialScrollIndex={indexes.minute - 1}
            data={timeData.minutes}
            onChange={minuteChanged}
            width={35}
          />
        </View>
      </View>
      {deletable && (
        <TouchableOpacity
          style={styles.deleteIconContainer}
          onPress={_onDelete}
        >
          <Image
            style={styles.deleteIcon}
            source={Icons.failed}
            resizeMode="contain"
          />
        </TouchableOpacity>
      )}
    </View>
  );
};

const indexOfStr = data => str => findIndex(equals(str), data);
const indexOfHour = indexOfStr(timeData.hours);
const indexOfMinute = indexOfStr(timeData.minutes);
const splitByColumn = split(":");

export default compose(
  withStateHandlers(({ dayTime }) => {
    const {
      days,
      todayIndex,
      firstDate,
      dayIndex
    } = dayHelpers.getDaysForDayTime(dayTime);

    return { days, firstDate, todayIndex, dayIndex };
  }, {}),
  withProps(({ dayTime, todayIndex, dayIndex }) => {
    const [hour, minute] = splitByColumn(dayTime.time);

    return {
      indexes: {
        hour: indexOfHour(hour),
        minute: indexOfMinute(minute),
        today: todayIndex,
        day: dayIndex
      }
    };
  }),
  withHandlers({
    hourChanged: props => hourIndex => {
      const { dayTime, indexes, onDayTimeChange } = props;
      const hour = timeData.hours[hourIndex];
      const minute = timeData.minutes[indexes.minute];
      const time = hour + ":" + minute;
      onDayTimeChange({ ...dayTime, time });
    },
    minuteChanged: props => minuteIndex => {
      const { dayTime, indexes, onDayTimeChange } = props;
      const hour = timeData.hours[indexes.hour];
      const minute = timeData.minutes[minuteIndex];
      const time = hour + ":" + minute;
      onDayTimeChange({ ...dayTime, time });
    },
    dayChanged: props => dayIndex => {
      const { dayTime, firstDate, onDayTimeChange } = props;
      const day = dayHelpers.indexToDay(dayIndex, firstDate);
      onDayTimeChange({ ...dayTime, day });
    },
    _onDelete: props => () => {
      const { dayTime, onDelete } = props;
      onDelete(dayTime);
    }
  })
)(DayTime);

const styles = StyleSheet.create({
  container: {
    marginVertical: 5
  },
  wheelTitle: {
    marginTop: 12,
    marginBottom: 8
  },
  wheels: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    borderRadius: 5,
    borderColor: Colors.GREY_LIGHTEST,
    borderWidth: 1,
    paddingHorizontal: 30,
    paddingBottom: 18
  },
  time: {
    display: "flex",
    flexDirection: "row"
  },
  column: {
    fontSize: 24,
    alignSelf: "center",
    marginHorizontal: 5
  },
  deleteIconContainer: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    top: theme.DELETE_CONTAINER.top,
    right: theme.DELETE_CONTAINER.right,
    width: theme.DELETE_CONTAINER.width,
    height: theme.DELETE_CONTAINER.height,
    borderRadius: theme.DELETE_CONTAINER.borderRadius,
    borderWidth: theme.DELETE_CONTAINER.borderWidth,
    borderColor: theme.DELETE_CONTAINER.borderColor,
    backgroundColor: theme.DELETE_CONTAINER.backgroundColor
  },
  deleteIcon: {
    width: theme.DELETE_ICON.width,
    height: theme.DELETE_ICON.height,
    tintColor: theme.DELETE_ICON.tintColor
  }
});
