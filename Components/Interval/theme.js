import { Colors } from "@Theme";

const ITEM_HEIGHT = 30;
const DELETE_WIDTH = 32;

export default {
  ITEM_HEIGHT,
  INTERVAL: {
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.GREY_LIGHT,
    marginBottom: 10
  },
  DELETE_CONTAINER: {
    top: ITEM_HEIGHT * 1.5 - DELETE_WIDTH / 2,
    right: -(DELETE_WIDTH / 2),
    width: DELETE_WIDTH,
    height: DELETE_WIDTH,
    borderRadius: DELETE_WIDTH / 2,
    borderWidth: 1,
    borderColor: Colors.GREY_LIGHT,
    backgroundColor: Colors.WHITE
  },
  DELETE_ICON: {
    width: 12,
    height: 12,
    tintColor: Colors.ERROR
  },
  WHEEL: {
    height: 90
    // width: 120,
  },
  ITEM: {
    fontSize: 24,
    fontWeight: "300",
    letterSpacing: 1,
    color: Colors.DARK
  }
};
