import { CustomText } from "@shared/Components";
import { Colors } from "@Theme";
import { equals, findIndex, split } from "ramda";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { compose, withHandlers, withProps, withStateHandlers } from "recompose";
import dayHelpers from "./dayHelpers";
import timeData from "./timeData";
import WheelPicker from "./WheelPicker";

const DateInterval = ({
  indexes,
  days,
  firstDate,
  startHourChanged,
  startMinuteChanged,
  endHourChanged,
  endMinuteChanged,
  startDayChanged,
  endDayChanged
}) => {
  return (
    <View style={styles.container}>
      <CustomText style={styles.wheelTitle}>From</CustomText>
      <View style={styles.wheels}>
        <WheelPicker
          initialScrollIndex={indexes.startDay}
          data={days}
          onChange={startDayChanged}
          width={135}
        />
        <View style={styles.time}>
          <WheelPicker
            initialScrollIndex={indexes.startHour - 1}
            data={timeData.hours}
            onChange={startHourChanged}
            width={30}
          />
          <Text style={styles.column}>:</Text>
          <WheelPicker
            initialScrollIndex={indexes.startMinute - 1}
            data={timeData.minutes}
            onChange={startMinuteChanged}
            width={30}
          />
        </View>
      </View>

      <CustomText style={styles.wheelTitle}>To</CustomText>
      <View style={styles.wheels}>
        <WheelPicker
          initialScrollIndex={indexes.endDay}
          data={days}
          onChange={endDayChanged}
          width={135}
        />
        <View style={styles.time}>
          <WheelPicker
            initialScrollIndex={indexes.endHour - 1}
            data={timeData.hours}
            onChange={endHourChanged}
            width={30}
          />
          <Text style={styles.column}>:</Text>
          <WheelPicker
            initialScrollIndex={indexes.endMinute - 1}
            data={timeData.minutes}
            onChange={endMinuteChanged}
            width={30}
          />
        </View>
      </View>
    </View>
  );
};

const indexOfStr = data => str => findIndex(equals(str), data);
const indexOfHour = indexOfStr(timeData.hours);
const indexOfMinute = indexOfStr(timeData.minutes);
const splitByColumn = split(":");

export default compose(
  withStateHandlers(({ interval }) => {
    const {
      days,
      todayIndex,
      firstDate,
      startDayIndex,
      endDayIndex
    } = dayHelpers.getDays(interval);

    return { days, firstDate, todayIndex, startDayIndex, endDayIndex };
  }, {}),
  withProps(({ interval, todayIndex, startDayIndex, endDayIndex }) => {
    const [startHour, startMinute] = splitByColumn(interval.startTime);
    const [endHour, endMinute] = splitByColumn(interval.endTime);

    return {
      indexes: {
        startHour: indexOfHour(startHour),
        startMinute: indexOfMinute(startMinute),
        endHour: indexOfHour(endHour),
        endMinute: indexOfMinute(endMinute),
        today: todayIndex,
        satartDay: startDayIndex,
        endDay: endDayIndex
      }
    };
  }),
  withHandlers({
    startHourChanged: props => startHourIndex => {
      const { interval, indexes, onIntervalChange } = props;
      const hour = timeData.hours[startHourIndex];
      const minute = timeData.minutes[indexes.startMinute];
      const startTime = hour + ":" + minute;
      onIntervalChange({ ...interval, startTime });
    },
    startMinuteChanged: props => startMinuteIndex => {
      const { interval, indexes, onIntervalChange } = props;
      const hour = timeData.hours[indexes.startHour];
      const minute = timeData.minutes[startMinuteIndex];
      const startTime = hour + ":" + minute;
      onIntervalChange({ ...interval, startTime });
    },
    endHourChanged: props => endHourIndex => {
      const { interval, indexes, onIntervalChange } = props;
      const hour = timeData.hours[endHourIndex];
      const minute = timeData.minutes[indexes.endMinute];
      const endTime = hour + ":" + minute;
      onIntervalChange({ ...interval, endTime });
    },
    endMinuteChanged: props => endMinuteIndex => {
      const { interval, indexes, onIntervalChange } = props;
      const hour = timeData.hours[indexes.endHour];
      const minute = timeData.minutes[endMinuteIndex];
      const endTime = hour + ":" + minute;
      onIntervalChange({ ...interval, endTime });
    },
    startDayChanged: props => startDayIndex => {
      const { interval, onIntervalChange, firstDate } = props;
      const startDay = dayHelpers.indexToDay(startDayIndex, firstDate);
      onIntervalChange({ ...interval, startDay });
    },
    endDayChanged: props => endDayIndex => {
      const { interval, onIntervalChange, firstDate } = props;
      const endDay = dayHelpers.indexToDay(endDayIndex, firstDate);
      onIntervalChange({ ...interval, endDay });
    }
  })
)(DateInterval);

const styles = StyleSheet.create({
  container: {
    // marginVertical: 18,
  },
  wheelTitle: {
    marginTop: 12,
    marginBottom: 8
  },
  wheels: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    borderRadius: 5,
    borderColor: Colors.GREY_LIGHTEST,
    borderWidth: 1,
    paddingHorizontal: 30,
    paddingBottom: 18
  },
  time: {
    display: "flex",
    flexDirection: "row"
  },
  column: {
    fontSize: 24,
    alignSelf: "center",
    marginHorizontal: 5
  }
});
