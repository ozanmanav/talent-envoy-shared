import { AnimatedText } from "@shared/Components";
import React from "react";
import { Animated, StyleSheet, View } from "react-native";
import theme from "./theme";
const { ITEM_HEIGHT } = theme;

export default class WheelPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0)
    };
    this.scrollRef = null;
  }

  /* Bu çare değil, bu bileşen sürekli didMount oluyor. */
  shouldComponentUpdate = (nextProps, nextState) => {
    return false;
  };

  setScrollRef = scrollRef => {
    this.scrollRef = scrollRef;
  };

  moveStopped = e => {
    const { y } = e.nativeEvent.contentOffset;
    const mod = y % 30;
    let offset = 0;

    if (mod > 10) offset = y - mod + 30;
    else offset = y - mod;

    this.scrollRef.getNode().scrollToOffset({ offset });
    const index = offset / ITEM_HEIGHT + 1;
    this.props.onChange(index);
  };

  onMomentumScrollEnd = e => {
    this.moveStopped(e);
  };

  onScrollEndDrag = e => {
    const velocityY = e.nativeEvent.velocity.y;
    if (velocityY === 0) {
      this.moveStopped(e);
    }
  };

  renderItem = ({ item, index }) => {
    const scrollRanges = [
      (index - 3) * ITEM_HEIGHT - 1,
      (index - 3) * ITEM_HEIGHT,
      (index - 1) * ITEM_HEIGHT,
      (index + 1) * ITEM_HEIGHT,
      (index + 1) * ITEM_HEIGHT + 1
    ];
    const scaleAnim = this.state.scrollY.interpolate({
      inputRange: scrollRanges,
      outputRange: [0.3, 0.3, 1, 0.3, 0.3]
    });
    const animStyle = {
      fontSize: theme.ITEM.fontSize,
      fontWeight: theme.ITEM.fontWeight,
      letterSpacing: theme.ITEM.letterSpacing,
      color: theme.ITEM.color,
      transform: [{ scaleY: scaleAnim }, { scaleX: scaleAnim }],
      opacity: scaleAnim
    };
    return (
      <View style={styles.itemContainer}>
        <AnimatedText type="black" style={animStyle}>
          {item}
        </AnimatedText>
      </View>
    );
  };

  render() {
    const { data, initialScrollIndex, width } = this.props;
    return (
      <View style={[styles.wheel, { width }]}>
        <Animated.FlatList
          ref={this.setScrollRef}
          style={styles.flatList}
          contentContainerStyle={styles.flatListContainer}
          data={data}
          // extraData={this.state.scrollY}
          keyExtractor={item => item}
          initialNumToRender={2}
          initialScrollIndex={initialScrollIndex}
          showsVerticalScrollIndicator={false}
          getItemLayout={(data, index) => ({
            length: ITEM_HEIGHT,
            offset: ITEM_HEIGHT * index,
            index
          })}
          renderItem={this.renderItem}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
            {
              useNativeDriver: true
            }
          )}
          onMomentumScrollEnd={this.onMomentumScrollEnd}
          onScrollEndDrag={this.onScrollEndDrag}
          nestedScrollEnabled={true}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wheel: {
    height: theme.WHEEL.height
  },
  flatList: {},
  flatListContainer: {},
  itemContainer: {
    height: ITEM_HEIGHT,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
});
