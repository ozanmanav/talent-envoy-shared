import { View, Text, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { Colors } from '@Theme'

class SuggestionListItem extends PureComponent {
  onPress = () => {
    const { name, id, onPressItem } = this.props
    onPressItem(id, name)
  }

  render() {
    const { name, textStyle } = this.props
    return (
      <TouchableOpacity onPress={this.onPress}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <Text style={[{ flex: 2, color: Colors.BLACK, fontSize: 16 }, textStyle]}>{name}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}
SuggestionListItem.propTypes = {
  textStyle: PropTypes.shape({}),
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onPressItem: PropTypes.func.isRequired,
}
SuggestionListItem.defaultProps = {
  textStyle: {},
}

export default SuggestionListItem
