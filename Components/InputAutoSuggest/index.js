import { FlatList, View, TextInput, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import SuggestionListItem from './SuggestionListItem'
import suggest from './services/suggest'

let style

class InputAutoSuggest extends Component {
  constructor(props) {
    super(props)
    this.state = { data: [], value: '' }

    this.searchList = this.searchList.bind(this)
    this.renderItem = this.renderItem.bind(this)
  }

  onPressItem = (id, name) => {
    const { onSubmitEditing, onChangeText } = this.props

    this.setState({
      value: name,
    })

    onChangeText(name)
    onSubmitEditing(name)
  }

  keyExtractor = (item) => item.id

  async searchList(text) {
    const { itemFormat, staticData } = this.props
    this.setState({ value: text })

    let suggestData = null
    if (staticData != null) {
      try {
        suggestData = suggest.searchForRelevant(text, staticData, itemFormat)
      } catch (e) {
        suggestData = { suggest: [], existingItem: null }
      }
    } else {
      suggestData = { suggest: [], existingItem: null }
    }

    this.setState({
      data: suggestData.suggest,
    })
  }

  renderItem = ({ item }) => {
    const { itemTextStyle } = this.props
    return (
      <SuggestionListItem
        textStyle={itemTextStyle}
        id={item.id}
        onPressItem={this.onPressItem}
        name={item.name}
      />
    )
  }

  render() {
    const { value, data } = this.state
    const {
      style,
      flatListStyle,
      setInputRef,
      blurOnSubmit,
      onFocus,
      onBlur,
      onSubmitEditing,
    } = this.props
    return (
      <View style={style.container}>
        <TextInput
          ref={setInputRef}
          style={style}
          value={value}
          clearButtonMode="while-editing"
          onChangeText={this.searchList}
          onSubmitEditing={onSubmitEditing}
          blurOnSubmit={blurOnSubmit}
          onFocus={onFocus}
          onBlur={onBlur}
        />
        {value !== '' && (
          <FlatList
            style={[style.flatList, flatListStyle]}
            data={data}
            extraData={this.state}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
          />
        )}
      </View>
    )
  }
}
InputAutoSuggest.propTypes = {
  style: PropTypes.shape({}),
  inputStyle: PropTypes.shape({}),
  flatListStyle: PropTypes.shape({}),
  itemTextStyle: PropTypes.shape({}),
  itemTagStyle: PropTypes.shape({}),
  apiEndpointSuggestData: PropTypes.func,
  staticData: PropTypes.arrayOf(PropTypes.shape({})),
  onDataSelectedChange: PropTypes.func,
  keyPathRequestResult: PropTypes.string,
  itemFormat: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }),
  setInputRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(InputAutoSuggest) }),
  ]),
  onSubmitEditing: PropTypes.func,
  onChangeText: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  blurOnSubmit: PropTypes.bool,
}
InputAutoSuggest.defaultProps = {
  inputStyle: {},
  flatListStyle: { paddingLeft: 20 },
  itemTextStyle: { fontSize: 14 },
  staticData: null,
  itemFormat: {
    id: 'id',
    name: 'name',
  },
}

style = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  input: {
    fontSize: 22,
    borderBottomWidth: 1,
  },
  flatList: {},
  itemTextStyle: { fontSize: 30 },
})

export default InputAutoSuggest
