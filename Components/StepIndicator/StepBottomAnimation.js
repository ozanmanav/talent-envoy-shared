import React from 'react'
import { View, LayoutAnimation, StyleSheet } from 'react-native'
import theme from './theme'

class StepBottomAnimation extends React.Component {
  constructor() {
    super()
    this.state = {
      x: 0,
      width: 0,
    }
  }
  componentDidMount() {
    const { x, width } = this.props
    this.setState({ x, width })
  }
  componentDidUpdate(prev) {
    const prevPosition = prev.position
    const { position } = this.props

    if (
      (prevPosition === undefined && position !== undefined) ||
      (prevPosition && prevPosition.x !== position.x)
    ) {
      LayoutAnimation.easeInEaseOut() // configureNext({ duration: 1000 })
      this.setState({ ...position })
    }
  }
  render() {
    const { x: left, width } = this.state
    const stepIndicatorAnimStyle = { left, width }
    const stepIndicatorStyle = [styles.stepIndicator, stepIndicatorAnimStyle]

    return <View style={stepIndicatorStyle} />
  }
}

export default StepBottomAnimation

const styles = StyleSheet.create({
  stepIndicator: {
    position: 'absolute',
    bottom: theme.STEP_INDICATOR.bottom,
    height: theme.STEP_INDICATOR.height,
    backgroundColor: theme.STEP_INDICATOR.backgroundColor,
  },
})
