import { CustomText } from '@shared/Components';
import { Icons } from "@Theme";
import React from "react";
import { Image, StyleSheet, TouchableOpacity, View } from "react-native";
import { withProps } from "recompose";
import theme from "./theme";


const Step = ({
  step: { text, topText },
  active,
  done,
  isLast,
  onLayout,
  onPressHandler,
  i
}) => {
  const activeStyle = active ? styles.active : styles.passive;
  const textStyle = [styles.text, activeStyle];
  const stepNoStyle = [styles.stepNo, activeStyle];
  const successStyle = [styles.success, activeStyle];

  return (
    <>
      <TouchableOpacity
        style={styles.container}
        onPress={onPressHandler}
        onLayout={onLayout}
      >
        <View style={styles.header}>
          <CustomText style={stepNoStyle}>{topText}</CustomText>
          {done && (
            <Image
              style={successStyle}
              source={Icons.success}
              resizeMode="contain"
            />
          )}
        </View>

        <CustomText style={textStyle}>{text}</CustomText>
      </TouchableOpacity>

      {!isLast && (
        <Image
          style={styles.saperator}
          source={Icons.right}
          resizeMode="contain"
        />
      )}
    </>
  );
};

export default withProps(({ onPress, done, i }) => ({
  onPressHandler: () => done && onPress(i)
}))(Step);

const styles = StyleSheet.create({
  container: {
    display: "flex",
    justifyContent: "space-between",
    paddingBottom: theme.STEP.paddingBottom,
    paddingTop: theme.STEP.paddingTop
  },
  saperator: {
    alignSelf: "flex-end",
    margin: theme.SAPERATOR.margin,
    marginBottom: theme.SAPERATOR.marginBottom,
    tintColor: theme.SAPERATOR.tintColor,
    opacity: theme.SAPERATOR.opacity,
    height: theme.SAPERATOR.height
  },
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  stepNo: {
    fontSize: theme.STEP_NO.fontSize,
    color: theme.STEP_NO.color
  },
  success: {
    height: theme.SUCCESS.height,
    tintColor: theme.SUCCESS.tintColor
  },
  text: {
    color: theme.TEXT.color,
    fontSize: 14
  },
  active: {
    opacity: theme.ACTIVE.opacity
  },
  passive: {
    opacity: theme.PASSIVE.opacity
  }
});
