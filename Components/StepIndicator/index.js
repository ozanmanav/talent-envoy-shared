import React from 'react'
import { View, StyleSheet } from 'react-native'
import { compose, withState, withHandlers } from 'recompose'
import Step from './Step'
import StepBottomAnimation from './StepBottomAnimation'
import theme from './theme'

const StepIndicator = ({ data, layouts, onLayout, stepPressed }) => (
  <View style={styles.container}>
    <View style={styles.content}>
      {data.steps.map((step, i) => (
        <Step
          key={i}
          i={i}
          onPress={stepPressed}
          isLast={i === data.steps.length - 1}
          step={step}
          active={i === data.currentIndex}
          done={i <= data.doneIndex}
          onLayout={onLayout(i)}
        />
      ))}
    </View>

    <View style={styles.bottomLine} />
    <StepBottomAnimation position={layouts[data.currentIndex]} />
  </View>
)

export default compose(
  withState('layouts', 'setLayouts', []),
  withHandlers({
    onLayout: ({ layouts, setLayouts }) => (i) => (e) => {
      const { width, x } = e.nativeEvent.layout
      const newLayouts = layouts
      newLayouts[i] = { width, x }
      setLayouts(newLayouts)
    },
  })
)(StepIndicator)

const styles = StyleSheet.create({
  bottomLine: {
    backgroundColor: theme.BOTTOM_LINE.backgroundColor,
    height: theme.BOTTOM_LINE.height,
    opacity: theme.BOTTOM_LINE.opacity,
  },
  container: {
    height: theme.CONTAINER.height,
    marginTop: theme.CONTAINER.marginTop,
  },
  content: {
    alignItems: 'stretch',
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
})
