import COLORS from 'App/Theme/Colors'

export default {
  CONTAINER: {
    height: 60,
    marginTop: 0,
  },
  BOTTOM_LINE: {
    height: 0.7,
    backgroundColor: COLORS.WHITE,
    opacity: 0.2,
  },
  STEP: {
    paddingBottom: 9,
    paddingTop: 6,
  },
  STEP_NO: {
    fontSize: 12,
    color: 'white',
  },
  SAPERATOR: {
    margin: 9,
    marginBottom: 14,
    tintColor: 'white',
    opacity: 0.5,
    height: 8,
  },
  SUCCESS: {
    height: 8,
    tintColor: 'white',
  },
  TEXT: {
    color: 'white',
  },
  ACTIVE: {
    opacity: 1,
  },
  PASSIVE: {
    opacity: 0.7,
  },
  STEP_INDICATOR: {
    bottom: 0,
    height: 1,
    backgroundColor: 'white',
  },
}
