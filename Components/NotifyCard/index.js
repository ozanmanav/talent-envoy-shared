import { CustomText } from '@shared/Components';
import { Icons } from '@Theme';
import PropTypes from 'prop-types';
import React from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import styles from './styles';

const NotifyCard = ({ title, detail, onPress, containerStyle }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.container, { containerStyle }]}>
        <View style={styles.textContainer}>
          <CustomText style={styles.title}>{title}</CustomText>
          <CustomText style={styles.detail}>{detail}</CustomText>
        </View>
        <Image
          source={Icons.arrowRight}
          style={{ width: 12, height: 12, resizeMode: 'contain', marginRight: 5 }}
        />
      </View>
    </TouchableOpacity>
  )
}

NotifyCard.propTypes = {
  title: PropTypes.string.isRequired,
  detail: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  containerStyle: PropTypes.object,
}
NotifyCard.defaultProps = {
  onPress: () => {},
}

export default NotifyCard
