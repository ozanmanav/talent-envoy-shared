import { StyleSheet } from 'react-native'
import { Colors } from '@Theme'

export default StyleSheet.create({
  container: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginHorizontal: 25,
    marginBottom: 15,
    borderRadius: 5,
    borderColor: Colors.GREY_LIGHT,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textContainer: { flex: 1, marginRight: 15 },
  title: {
    fontSize: 14,
    color: Colors.DARKER,
    fontWeight: '500',
  },
  detail: {
    fontSize: 14,
    color: Colors.DARK,
  },
})
