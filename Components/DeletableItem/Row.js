import { CustomText } from '@shared/Components';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import Delete from './Delete';
import theme from './theme';

const Row = ({ label, index, onDelete }) => {
  return (
    <View style={styles.container}>
      <View style={styles.left}>
        <View style={styles.bullet} />
        <CustomText type="book" style={styles.label}>
          {label}
        </CustomText>
      </View>

      <Delete index={index} onDelete={onDelete} />
    </View>
  )
}

export default Row

const styles = StyleSheet.create({
  bullet: {
    backgroundColor: theme.ROW.BULLET.backgroundColor,
    borderRadius: theme.ROW.BULLET.width / 2,
    height: theme.ROW.BULLET.width,
    width: theme.ROW.BULLET.width,
  },
  container: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: theme.ROW.CONTAINER.marginVertical,
  },
  label: {
    color: theme.ROW.LABEL.color,
    marginLeft: theme.ROW.LABEL.marginLeft,
  },
  left: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
  },
})
