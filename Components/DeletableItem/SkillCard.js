import { CustomText, Slider } from '@shared/Components';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import Delete from './Delete';
import theme from './theme';


const SkillCard = ({ label, minValue, maxValue, level, onLevelChange, index, onDelete }) => {
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <CustomText style={styles.label}>{label}</CustomText>
        <Delete index={index} onDelete={onDelete} />
      </View>

      <Slider
        title="RATE YOUR EXPERTISE LEVEL"
        initialValue={level}
        minValue={1}
        maxValue={10}
        onChange={onLevelChange}
      />
    </View>
  )
}

export default SkillCard

const styles = StyleSheet.create({
  container: {
    borderColor: theme.SKILL_CARD.borderColor,
    borderWidth: 1,
    borderRadius: theme.SKILL_CARD.borderRadius,
    padding: 20,
    marginBottom: theme.SKILL_CARD.marginBottom,
  },
  top: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  label: {
    fontSize: theme.SKILL_CARD.LABEL.fontSize,
    color: theme.SKILL_CARD.LABEL.color,
  },
})
