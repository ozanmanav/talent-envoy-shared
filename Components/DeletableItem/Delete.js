import React from 'react'
import { TouchableOpacity, StyleSheet, Image } from 'react-native'
import { Icons } from '@Theme'
import theme from './theme'

const Delete = ({ onDelete, index }) => {
  return (
    <TouchableOpacity style={styles.deleteContainer} onPress={() => onDelete(index)}>
      <Image style={styles.icon} source={Icons.failed} resizeMode="contain" />
    </TouchableOpacity>
  )
}

export default Delete

const styles = StyleSheet.create({
  deleteContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
  icon: {
    height: theme.DELETE_ICON.width,
    margin: theme.DELETE_ICON.margin,
    width: theme.DELETE_ICON.width,
  },
})
