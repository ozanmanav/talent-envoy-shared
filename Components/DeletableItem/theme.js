import { Colors } from '@Theme'

const theme = {
  ROW: {
    CONTAINER: {
      marginVertical: 5,
    },
    BULLET: {
      width: 6,
      backgroundColor: Colors.DARK,
    },
    LABEL: {
      marginLeft: 14,
      color: Colors.DARK,
    },
  },
  CHIP: {
    CONTAINER: {
      marginVertical: 4,
      marginRight: 4,
      height: 30,
      paddingHorizontal: 8,
      backgroundColor: Colors.SECONDARY_LIGHT,
    },
    LABEL: {
      color: Colors.SECONDARY,
      marginHorizontal: 4,
      marginBottom: 1,
    },
    ICON: {
      tintColor: Colors.SECONDARY,
      width: 10,
      margin: 3,
    },
  },
  SKILL_CARD: {
    borderColor: Colors.GREY_LIGHTEST,
    borderRadius: 5,
    marginBottom: 8,
    LABEL: {
      fontSize: 18,
      color: Colors.DARKER,
    },
  },
  DELETE_ICON: {
    width: 12,
    margin: 3,
  },
}

export default theme
