import { CustomText } from '@shared/Components';
import { Icons } from '@Theme';
import React from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import theme from './theme';

const Chip = ({ type, label, index, onDelete }) => {
  return (
    <View style={styles.container}>
      <CustomText type="book" style={styles.label}>
        {label}
      </CustomText>

      <TouchableOpacity style={styles.deleteContainer} onPress={() => onDelete(index)}>
        <Image style={styles.icon} source={Icons.failed} resizeMode="contain" />
      </TouchableOpacity>
    </View>
  )
}

export default Chip

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: theme.CHIP.CONTAINER.backgroundColor,
    borderRadius: theme.CHIP.CONTAINER.height / 2,
    display: 'flex',
    flexDirection: 'row',
    height: theme.CHIP.CONTAINER.height,
    marginRight: theme.CHIP.CONTAINER.marginRight,
    marginVertical: theme.CHIP.CONTAINER.marginVertical,
    paddingHorizontal: theme.CHIP.CONTAINER.paddingHorizontal,
  },
  deleteContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
  icon: {
    height: theme.CHIP.ICON.width,
    margin: theme.CHIP.ICON.margin,
    tintColor: theme.CHIP.ICON.tintColor,
    width: theme.CHIP.ICON.width,
  },
  label: {
    color: theme.CHIP.LABEL.color,
    marginBottom: theme.CHIP.LABEL.marginBottom,
    marginHorizontal: theme.CHIP.LABEL.marginHorizontal,
  },
})
