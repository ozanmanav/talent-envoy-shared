import { CustomText } from '@shared/Components';
import { Colors } from "@Theme";
import PropTypes from "prop-types";
import React from "react";
import { StyleSheet, View } from "react-native";

const TitleWithDetail = ({ title, detail }) => (
  <View style={styles.container}>
    <CustomText
      style={{ fontSize: 22, color: Colors.WHITE, marginBottom: 10 }}
      type="book"
    >
      {title}
    </CustomText>
    <CustomText style={{ fontSize: 14, color: Colors.WHITE }} type="book">
      {detail}
    </CustomText>
  </View>
);

export default TitleWithDetail;

TitleWithDetail.propTypes = {
  title: PropTypes.string.isRequired,
  detail: PropTypes.string.isRequired
};

const styles = StyleSheet.create({
  container: {
    padding: 30,
    paddingRight: 20
  }
});
