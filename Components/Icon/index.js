import React from "react";
import { Image } from "react-native";
import PropTypes from "prop-types";
import styles from "./styles";

const Icon = ({ iconSource, iconWidth, iconHeight, tintColor }) => {
  return (
    <Image
      tintColor={tintColor}
      source={iconSource}
      style={[
        styles.image,
        {
          tintColor,
          width: iconWidth,
          height: iconHeight,
          tintColor
        }
      ]}
    />
  );
};

Icon.propTypes = {
  tintColor: PropTypes.string,
  iconSource: PropTypes.number.isRequired,
  iconWidth: PropTypes.number,
  iconHeight: PropTypes.number
};

Icon.defaultProps = {
  iconWidth: 100,
  iconHeight: 100
};

export default Icon;
