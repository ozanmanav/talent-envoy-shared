import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  image: { resizeMode: 'contain', alignSelf: 'center', marginVertical: 10 },
})
