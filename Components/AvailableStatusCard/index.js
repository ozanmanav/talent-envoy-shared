import { CustomText } from "@shared/Components";
import { Colors, Icons } from "@Theme";
import React from "react";
import { Image } from "react-native";

const AvailableStatusCard = ({ isOnline }) => {
  return (
    <>
      <Image
        source={Icons.clock}
        style={[
          {
            width: 17,
            height: 17,
            tintColor: isOnline ? Colors.SUCCESS : Colors.ERROR,
            marginRight: 3
          }
        ]}
      />
      <CustomText
        style={{
          color: isOnline ? Colors.SUCCESS : Colors.ERROR,
          fontSize: 14,
          fontWeight: "500",
          marginLeft: 5
        }}
      >
        {isOnline ? "Available" : "Offline"}
      </CustomText>
    </>
  );
};

AvailableStatusCard.propTypes = {};

export default AvailableStatusCard;
