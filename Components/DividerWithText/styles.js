import { StyleSheet } from 'react-native'
import { Colors } from '@Theme'

export default StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 5,
  },
  line: {
    flex: 1,
    height: 1,
    backgroundColor: '#dcdcdc',
  },
  text: {
    fontSize: 14,
    color: Colors.DARK,
    marginHorizontal: 10,
    fontWeight: '500',
  },
})
