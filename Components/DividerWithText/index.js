import React from "react";
import { View } from "react-native";
import { CustomText } from "../index";
import styles from "./styles";

const DividerWithText = ({ text = "OR" }) => (
  <View style={styles.container}>
    <View style={styles.line} />
    <CustomText style={styles.text}>{text}</CustomText>
    <View style={styles.line} />
  </View>
);

export default DividerWithText;
