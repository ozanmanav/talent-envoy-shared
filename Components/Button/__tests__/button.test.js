import React from 'react'
import Button from '../index'
import renderer from 'react-test-renderer'
test('renders correctly', () => {
  const tree = renderer.create(<Button title="selam" type="primary" />).toJSON()
  expect(tree).toMatchSnapshot()
})
