import { Colors } from "@Theme";

export default {
  CONTAINER: {
    height: 50,
    secondaryHeight: 50,
    borderRadius: 5,
    marginVertical: {
      _PRIMARY: 10,
      _SECONDARY: 10,
      _LINK: 10,
      _SECONDARYLINK: 10
    },
    backgroundColor: {
      _PRIMARY: Colors.PRIMARY,
      _PRIMARY_PRESSED: "rgba(245,85,43,0.2)",
      _PRIMARY_DEACTIVE: Colors.GREY_LIGHT,
      _SECONDARY: Colors.SECONDARY_LIGHT,
      _SECONDARY_PRESSED: "rgba(217,248,255,0.2)",
      _SECONDARY_DEACTIVE: Colors.GREY_LIGHT,
      _LINK: Colors.WHITE,
      _SECONDARYLINK: Colors.WHITE
    }
  },
  TITLE: {
    fontWeight: "500",
    fontSize: {
      _PRIMARY: 16,
      _SECONDARY: 16,
      _LINK: 14,
      _SECONDARYLINK: 14
    },
    color: {
      _PRIMARY: Colors.WHITE,
      _PRIMARY_PRESSED: Colors.WHITE,
      _PRIMARY_DEACTIVE: Colors.DARK_GREY,
      _SECONDARY: Colors.SECONDARY,
      _SECONDARY_PRESSED: Colors.SECONDARY,
      _SECONDARY_DEACTIVE: Colors.DARK_GREY,
      _LINK: Colors.SECONDARY,
      _LINK_PRESSED: Colors.TERTIARY,
      _LINK_DEACTIVE: Colors.SECONDARY,
      _SECONDARYLINK: Colors.PRIMARY,
      _SECONDARYLINK_PRESSED: Colors.TERTIARY,
      _SECONDARYLINK_DEACTIVE: Colors.SECONDARY
    },
    opacity: {
      _LINK_DEACTIVE: 0.4,
      _SECONDARYLINK_DEACTIVE: 0.4
    }
  }
};
