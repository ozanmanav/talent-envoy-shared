import { StyleSheet } from "react-native";
import theme from "./theme";

const commonStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  // marginTop: theme.CONTAINER.marginTop,
  height: theme.CONTAINER.height,
  borderRadius: theme.CONTAINER.borderRadius
};

export default StyleSheet.create({
  container_primary_normal: {
    ...commonStyle,
    backgroundColor: theme.CONTAINER.backgroundColor._PRIMARY,
    marginVertical: theme.CONTAINER.marginVertical._PRIMARY
  },
  container_primary_pressed: {
    ...commonStyle,
    backgroundColor: theme.CONTAINER.backgroundColor._PRIMARY_PRESSED,
    marginVertical: theme.CONTAINER.marginVertical._PRIMARY
  },
  container_primary_deactive: {
    ...commonStyle,
    backgroundColor: theme.CONTAINER.backgroundColor._PRIMARY_DEACTIVE,
    marginVertical: theme.CONTAINER.marginVertical._PRIMARY
  },
  container_secondary_normal: {
    ...commonStyle,
    height: theme.CONTAINER.secondaryHeight,
    backgroundColor: theme.CONTAINER.backgroundColor._SECONDARY,
    marginVertical: theme.CONTAINER.marginVertical._SECONDARY
  },
  container_secondary_pressed: {
    ...commonStyle,
    height: theme.CONTAINER.secondaryHeight,
    backgroundColor: theme.CONTAINER.backgroundColor._SECONDARY_PRESSED,
    marginVertical: theme.CONTAINER.marginVertical._SECONDARY
  },
  container_secondary_deactive: {
    ...commonStyle,
    height: theme.CONTAINER.secondaryHeight,
    backgroundColor: theme.CONTAINER.backgroundColor._SECONDARY_DEACTIVE,
    marginVertical: theme.CONTAINER.marginVertical._SECONDARY
  },
  container_link_normal: {
    ...commonStyle,
    height: 18,
    backgroundColor: theme.CONTAINER.backgroundColor._LINK,
    marginVertical: theme.CONTAINER.marginVertical._LINK
  },
  container_link_pressed: {
    ...commonStyle,
    height: 18,
    backgroundColor: theme.CONTAINER.backgroundColor._LINK,
    marginVertical: theme.CONTAINER.marginVertical._LINK
  },
  container_link_deactive: {
    ...commonStyle,
    height: 18,
    backgroundColor: theme.CONTAINER.backgroundColor._LINK,
    marginVertical: theme.CONTAINER.marginVertical._LINK
  },
  container_secondarylink_normal: {
    ...commonStyle,
    height: 18,
    backgroundColor: theme.CONTAINER.backgroundColor._SECONDARYLINK,
    marginVertical: theme.CONTAINER.marginVertical._SECONDARYLINK
  },
  container_secondarylink_pressed: {
    ...commonStyle,
    height: 18,
    backgroundColor: theme.CONTAINER.backgroundColor._SECONDARYLINK,
    marginVertical: theme.CONTAINER.marginVertical._SECONDARYLINK
  },
  container_secondarylink_deactive: {
    ...commonStyle,
    height: 18,
    backgroundColor: theme.CONTAINER.backgroundColor._SECONDARYLINK,
    marginVertical: theme.CONTAINER.marginVertical._SECONDARYLINK
  },

  title_primary_normal: {
    fontSize: theme.TITLE.fontSize._PRIMARY,
    color: theme.TITLE.color._PRIMARY,
    fontWeight: theme.TITLE.fontWeight
  },
  title_primary_pressed: {
    fontSize: theme.TITLE.fontSize._PRIMARY,
    color: theme.TITLE.color._PRIMARY_PRESSED,
    fontWeight: theme.TITLE.fontWeight
  },
  title_primary_deactive: {
    fontSize: theme.TITLE.fontSize._PRIMARY,
    color: theme.TITLE.color._PRIMARY_DEACTIVE,
    fontWeight: theme.TITLE.fontWeight
  },
  title_secondary_normal: {
    fontSize: theme.TITLE.fontSize._SECONDARY,
    color: theme.TITLE.color._SECONDARY,
    fontWeight: theme.TITLE.fontWeight
  },
  title_secondary_pressed: {
    fontSize: theme.TITLE.fontSize._SECONDARY,
    color: theme.TITLE.color._SECONDARY_PRESSED,
    fontWeight: theme.TITLE.fontWeight
  },
  title_secondary_deactive: {
    fontSize: theme.TITLE.fontSize._SECONDARY,
    color: theme.TITLE.color._SECONDARY_DEACTIVE,
    fontWeight: theme.TITLE.fontWeight
  },
  title_link_normal: {
    fontSize: theme.TITLE.fontSize._LINK,
    color: theme.TITLE.color._LINK,
    fontWeight: theme.TITLE.fontWeight
  },
  title_link_pressed: {
    fontSize: theme.TITLE.fontSize._LINK,
    color: theme.TITLE.color._LINK_PRESSED,
    fontWeight: theme.TITLE.fontWeight
  },
  title_link_deactive: {
    fontSize: theme.TITLE.fontSize._LINK,
    color: theme.TITLE.color._LINK_DEACTIVE,
    opacity: theme.TITLE.opacity._LINK_DEACTIVE,
    fontWeight: theme.TITLE.fontWeight
  },
  title_secondarylink_normal: {
    fontSize: theme.TITLE.fontSize._SECONDARYLINK,
    color: theme.TITLE.color._SECONDARYLINK,
    fontWeight: theme.TITLE.fontWeight
  },
  title_secondarylink_pressed: {
    fontSize: theme.TITLE.fontSize._SECONDARYLINK,
    color: theme.TITLE.color._SECONDARYLINK_PRESSED,
    fontWeight: theme.TITLE.fontWeight
  },
  title_secondarylink_deactive: {
    fontSize: theme.TITLE.fontSize._SECONDARYLINK,
    color: theme.TITLE.color._SECONDARYLINK_DEACTIVE,
    opacity: theme.TITLE.opacity._SECONDARYLINK_DEACTIVE,
    fontWeight: theme.TITLE.fontWeight
  }
});
