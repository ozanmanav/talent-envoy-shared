import { CustomText } from '@shared/Components';
import PropTypes from 'prop-types';
import React from 'react';
import { TouchableWithoutFeedback, View } from 'react-native';
import { compose, withStateHandlers } from 'recompose';
import styles from './styles';

const Button = ({
  type,
  deactive,
  title,
  onPress,
  isPressedIn,
  onPressIn,
  onPressOut,
  containerStyle,
  testID,
}) => {
  const styleState = deactive ? 'deactive' : isPressedIn ? 'pressed' : 'normal'
  const containerClass = 'container_' + type + '_' + styleState
  const titleClass = 'title_' + type + '_' + styleState

  return (
    <TouchableWithoutFeedback
      testID={testID}
      disabled={deactive}
      onPress={onPress}
      onPressIn={onPressIn}
      onPressOut={onPressOut}
    >
      <View style={[styles[containerClass], containerStyle]}>
        <CustomText style={styles[titleClass]}>{title}</CustomText>
      </View>
    </TouchableWithoutFeedback>
  )
}

export default compose(
  withStateHandlers(
    { isPressedIn: false },
    {
      onPressIn: (state, { form }) => () => {
        if (form && form.getState().active) form.blur(form.getState().active)

        return { isPressedIn: true }
      },
      onPressOut: () => () => {
        return { isPressedIn: false }
      },
    }
  )
)(Button)

Button.propTypes = {
  type: PropTypes.string.isRequired,
  deactive: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  isPressedIn: PropTypes.bool.isRequired,
  onPressIn: PropTypes.func.isRequired,
  onPressOut: PropTypes.func.isRequired,
  containerStyle: PropTypes.object.isRequired,
  testID: PropTypes.string,
}

Button.defaultProps = {
  // type: 'primary',
  deactive: false,
  // title: 'Button Title',
  onPress: () => {},
  // isPressedIn: false,
  // onPressIn: ()=>{},
  // onPressOut: ()=>{},
  containerStyle: {},
}
