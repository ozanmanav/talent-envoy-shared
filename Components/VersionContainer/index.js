import { CustomText } from "@shared/Components";
import React from "react";
import styles from "./styles";

const VersionContainer = ({ version, buildNumber, serverURI }) => {
  return (
    <CustomText style={styles.versionContainer}>
      {version} ({buildNumber}){serverURI?.includes("192") ? " D" : null}
      {serverURI?.includes("staging") ? " S" : null}
      {serverURI?.includes("prod") ? " P" : null}
    </CustomText>
  );
};

export default VersionContainer;
