import { Colors } from "@Theme";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  versionContainer: {
    textAlign: "center",
    color: Colors.DARK_GREY,
    fontSize: 8
  }
});
