import { CustomText } from "@shared/Components";
import { Colors, Icons } from "@Theme";
import PropTypes from "prop-types";
import React from "react";
import { Image, StyleSheet, TouchableOpacity, View } from "react-native";
import { withHandlers } from "recompose";

const NavigationBar2 = ({ hasBack, _onBack, title, renderRight }) => {
  const rightType = typeof renderRight;
  const right =
    rightType === "undefined" ? (
      <View />
    ) : rightType === "object" ? (
      renderRight
    ) : (
      renderRight()
    );
  return (
    <View style={styles.container}>
      {hasBack && (
        <TouchableOpacity style={styles.backContainer} onPress={_onBack}>
          <Image source={Icons.left} style={styles.backIcon} />
        </TouchableOpacity>
      )}

      {title && <CustomText style={styles.title}>{title}</CustomText>}

      {right}
    </View>
  );
};

const _NavigationBar2 = withHandlers({
  _onBack: ({ hasBack, onBack }) => () => {
    if (hasBack) onBack();
  }
})(NavigationBar2);

export default _NavigationBar2;

_NavigationBar2.propTypes = {
  hasBack: PropTypes.bool,
  onBack: PropTypes.func,
  title: PropTypes.string,
  renderRight: PropTypes.oneOfType([PropTypes.element, PropTypes.elementType])
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: 60,
    paddingHorizontal: 15
  },
  backContainer: {},
  backIcon: {
    width: 16,
    height: 16,
    tintColor: Colors.WHITE
  },
  title: {
    fontSize: 18,
    color: Colors.WHITE,
    marginHorizontal: 8
  }
});
