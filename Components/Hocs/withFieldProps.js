import { withProps } from 'recompose'

const defaultTo = (var1, var2) => (typeof var1 === 'undefined' ? var2 : var1)

// controlledValue is actually value. But value is I guess reserved variable and final form overrides it.

export default withProps(
  ({ onChangeText, onBlur, onFocus, controlledValue, isFocused, hasError, input, meta }) => ({
    onChangeText: defaultTo(onChangeText, input && input.onChange),
    onBlur: defaultTo(onBlur, input && input.onBlur),
    onFocus: defaultTo(onFocus, input && input.onFocus),
    value: defaultTo(controlledValue, input && input.value),
    isFocused: defaultTo(isFocused, meta && meta.active),
    hasError: defaultTo(hasError, Boolean(meta) && Boolean(meta.error) && Boolean(meta.touched)),
  })
)
