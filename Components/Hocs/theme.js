export default {
  TITLED: {
    CONTAINER: {
      marginVertical: 10,
    },
    TITLE: {
      marginVertical: 8,
      fontSize: 12,
      fontWeight: 'bold',
      color: '#aaaaaa',
    },
  },
}
