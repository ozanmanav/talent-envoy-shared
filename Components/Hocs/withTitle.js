import { CustomText } from '@shared/Components';
import React from 'react';
import { View } from 'react-native';
import styles from './styles';

export default (Component) => ({ title, ...props }) => (
  <View style={styles.container}>
    {title && <CustomText style={styles.title}>{title}</CustomText>}
    <Component {...props} />
  </View>
)
