import { withProps } from 'recompose'

const defaultTo = (var1, var2) => (typeof var1 === 'undefined' ? var2 : var1)

export default withProps(({ name, val, onSelected, selected, input, meta }) => ({
  name: defaultTo(name, input && input.name),
  onSelected: defaultTo(onSelected, input && input.onChange),
  selected: defaultTo(selected, input && input.value === val),
}))
