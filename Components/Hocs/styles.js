import { StyleSheet } from 'react-native'
import theme from './theme'

export default StyleSheet.create({
  container: {
    marginVertical: theme.TITLED.CONTAINER.marginVertical,
  },
  title: {
    marginVertical: theme.TITLED.TITLE.marginVertical,
    fontSize: theme.TITLED.TITLE.fontSize,
    fontWeight: theme.TITLED.TITLE.fontWeight,
    color: theme.TITLED.TITLE.color,
  },
})
