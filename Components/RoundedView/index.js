import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Colors } from '@Theme'

const RoundedView = ({ children, style }) => (
  <View style={[styles.container, style]}>{children}</View>
)

export default RoundedView

const styles = StyleSheet.create({
  container: {
    alignItems: 'stretch',
    backgroundColor: Colors.WHITE,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    display: 'flex',
    flex: 3,
    justifyContent: 'flex-start',
    paddingHorizontal: 30,
    paddingTop: 20,
  },
})
