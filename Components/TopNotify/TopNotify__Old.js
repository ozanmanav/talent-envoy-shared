import { CustomText } from '@shared/Components';
import PropTypes from 'prop-types';
import React from 'react';
import { StatusBar, View } from 'react-native';
import styles from './styles';

const TopNotify = ({ type, visible, text }) => {
  const containerStyle = type === 'error' ? styles.containerError : styles.containerSuccess
  return visible ? (
    <View style={containerStyle}>
      <StatusBar hidden />
      <CustomText style={styles.text}>{text}</CustomText>
    </View>
  ) : null
}

export default TopNotify

TopNotify.propTypes = {
  type: PropTypes.oneOf(['error', 'success']).isRequired,
  visible: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
}
