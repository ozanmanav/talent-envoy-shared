import React from "react";
import { showMessage, hideMessage } from "../ReactNativeFlashMessage";
import { compose, any, identity, values, path } from "ramda";

const anyPropTrue = compose(
  any(identity),
  values
);

const firstOfFirst = compose(
  path([0, 0]),
  values
);

export default class FirstError extends React.Component {
  static getDerivedStateFromProps(props, state) {
    const { errors, touched, active } = props;
    const show = anyPropTrue(touched);
    const message = active ? path([active, 0], errors) : firstOfFirst(errors);
    return { show, message };
  }
  componentDidUpdate(prevProps, prevState) {
    const { show: show1, message: message1 } = prevState;
    const { show: show2, message: message2 } = this.state;

    if (show1 === show2 && message1 === message2) {
    } else if ((show1 && !show2) || !message2) hideMessage();
    else if (show2)
      showMessage({ message: message2, type: "danger", autoHide: false });
  }
  componentWillUnmount() {
    hideMessage();
  }
  render() {
    return null;
  }
}
