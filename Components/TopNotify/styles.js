import { StyleSheet } from 'react-native'

const common = {
  position: 'absolute',
  top: 0,
  left: 0,
  right: 0,
  height: 28,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  zIndex: 999,
}

export default StyleSheet.create({
  containerError: {
    ...common,
    backgroundColor: 'red',
  },
  containerSuccess: {
    ...common,
    backgroundColor: 'green',
  },
  text: {
    fontSize: 14,
    color: 'white',
  },
})
