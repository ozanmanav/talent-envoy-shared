import { compose, mapProps } from 'recompose'
import withModalStateHandlers from './withModalStateHandlers'

export default (name) =>
  compose(
    withModalStateHandlers,
    mapProps(({ isModalVisible, openModal, closeModal, ...props }) => ({
      [name]: { isModalVisible, openModal, closeModal },
      ...props,
    }))
  )
