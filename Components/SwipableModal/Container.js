import React from "react";
import { StyleSheet } from "react-native";
import Modal from "../ReactNativeModal";

const CLOSE_GAP = 100;

class Container extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDragging: false,
      attatchToTop: true
    };
  }

  onScrollBeginDrag = e => {
    this.setState({ isDragging: true });
  };
  onScrollEndDrag = e => {
    this.setState({ isDragging: false });
    if (e.nativeEvent.contentOffset.y < CLOSE_GAP) this.props.closeModal();
  };
  onScroll = e => {
    const { attatchToTop, isDragging } = this.state;
    const y = e.nativeEvent.contentOffset.y;

    if (y < CLOSE_GAP && attatchToTop && !isDragging) this.onCloseAreaReached();
  };
  onCloseAreaReached = () => {
    this.setState({ attatchToTop: false });
    setTimeout(() => {
      const { componentType } = this.props;
      if (this.scrollRef != null) {
        try {
          if (componentType === "flatList")
            this.scrollRef.scrollToOffset({ offset: CLOSE_GAP });
          else this.scrollRef.scrollTo({ y: CLOSE_GAP });
        } catch (error) {
          console.log(error);
        }
      }
    }, 100);
    setTimeout(() => {
      this.setState({ attatchToTop: true });
    }, 250);
  };

  scrollRef = null;

  setScrollRef = scrollRef => {
    this.scrollRef = scrollRef;
  };

  render() {
    const { isModalVisible, render, onModalClose } = this.props;
    const { isDragging, attatchToTop } = this.state;

    const scrollProps = {
      ref: this.setScrollRef,
      contentOffset: { y: CLOSE_GAP },
      scrollEventThrottle: 40,
      bounces: false,
      onScrollBeginDrag: this.onScrollBeginDrag,
      onScrollEndDrag: this.onScrollEndDrag,
      onScroll: this.onScroll
    };

    return (
      <Modal
        isVisible={isModalVisible}
        style={styles.modal}
        onModalHide={onModalClose}
      >
        {render(scrollProps)}
      </Modal>
    );
  }
}

export default Container;

const styles = StyleSheet.create({
  modal: {
    margin: 0
  },
  scrollView: {},
  scrollViewContentContainer: {
    paddingTop: 400
  }
});
