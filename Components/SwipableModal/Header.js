import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Colors } from '@Theme'

const SwipableModalHeader = ({ children }) => (
  <View style={styles.swipeBar}>
    <View style={styles.swipeLine} />
    {children}
  </View>
)

export default SwipableModalHeader

const styles = StyleSheet.create({
  swipeBar: {
    display: 'flex',
    backgroundColor: Colors.WHITE,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  swipeLine: {
    alignSelf: 'center',
    backgroundColor: Colors.GREY_LIGHT,
    borderRadius: 3,
    height: 6,
    width: 70,
    margin: 10,
    marginBottom: 0,
  },
})
