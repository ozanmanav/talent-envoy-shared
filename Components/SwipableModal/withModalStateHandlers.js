import { withStateHandlers } from 'recompose'

export default withStateHandlers(
  { isModalVisible: false },
  {
    openModal: () => () => ({ isModalVisible: true }),
    closeModal: () => () => ({ isModalVisible: false }),
  }
)
