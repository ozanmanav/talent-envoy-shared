import React from 'react'
import { View } from 'react-native'
import { Colors } from '@Theme'

const ItemSeperator = () => (
  <View
    style={{
      height: 1,
      width: '100%',
      backgroundColor: Colors.GREY_LIGHT,
    }}
  />
)

export default ItemSeperator
