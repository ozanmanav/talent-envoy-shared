/* eslint-disable camelcase */
import React, { Component } from "react";
import { Animated, Easing } from "react-native";
import { Images, Icons } from "@Theme";
import PropTypes from "prop-types";
import COLORS from "App/Theme/Colors";
import CircledIcon from "../CircledIcon";
import styles from "./styles";

class ConditionalLoading extends Component {
  state = {
    rotateValue: new Animated.Value(0)
  };

  componentDidMount() {
    this.startAnimation();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.isLoading) {
      this.startAnimation();
    }
  }

  startAnimation() {
    this.state.rotateValue.setValue(0);

    Animated.loop(
      Animated.timing(this.state.rotateValue, {
        toValue: 1,
        duration: 1000,
        easing: Easing.linear
      }),
      {
        // iterations: 3
      }
    ).start();
  }

  render() {
    const { isLoading, result } = this.props;
    if (isLoading) {
      return (
        <Animated.Image
          source={Images.loadingIcon}
          style={[
            styles.image,
            {
              tintColor: this.props.tintColor,
              transform: [
                {
                  rotate: this.state.rotateValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: ["360deg", "0deg"]
                  })
                }
              ]
            }
          ]}
        />
      );
    }
    if (!isLoading && result) {
      return (
        <CircledIcon
          icon={Icons.success}
          color={COLORS.SUCCESS}
          iconWidth={18}
          iconHeight={13}
        />
      );
    }
    if (!isLoading && !result) {
      return (
        <CircledIcon
          icon={Icons.failed}
          color={COLORS.ERROR}
          iconWidth={14}
          iconHeight={14}
        />
      );
    }
    return null;
  }
}

ConditionalLoading.propTypes = {
  tintColor: PropTypes.string,
  isLoading: PropTypes.bool,
  result: PropTypes.bool
};

ConditionalLoading.defaultProps = {
  tintColor: COLORS.PRIMARY,
  isLoading: false,
  result: null
};

export default ConditionalLoading;
