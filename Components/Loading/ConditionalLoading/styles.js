import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  image: {
    width: 50,
    height: 50,
    alignSelf: 'center',
    marginVertical: 10,
  },
})
