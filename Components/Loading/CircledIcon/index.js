import React from 'react'
import { View, Image } from 'react-native'
import PropTypes from 'prop-types'
import styles from './styles'

const CircledIcon = ({ color, icon, iconWidth, iconHeight }) => {
  return (
    <View style={[styles.container, { borderColor: color }]}>
      <Image
        source={icon}
        style={{ width: iconWidth, height: iconHeight, tintColor: color, resizeMode: 'contain' }}
      />
    </View>
  )
}

CircledIcon.propTypes = {
  color: PropTypes.string.isRequired,
  icon: PropTypes.number.isRequired,
  iconWidth: PropTypes.number,
  iconHeight: PropTypes.number,
}

CircledIcon.defaultProps = {
  iconWidth: 18,
  iconHeight: 18,
}

export default CircledIcon
