import { CustomText } from "@shared/Components";
import { Colors, Icons } from "@Theme";
import PropTypes from "prop-types";
import React from "react";
import { Image, TouchableOpacity, View } from "react-native";
import AvailableStatusCard from "../AvailableStatusCard";
import Icon from "../Icon";
import styles from "./styles";

const NavigationBar = ({
  hasBack,
  title,
  titleLeft,
  iconRight,
  titleRight,
  onBackButtonPress,
  iconRightStyle,
  showAvailableStatus,
  isOnline,
  showFilterButton,
  filterButtonPress,
  onRightPress
}) => (
  <View style={styles.container}>
    <TouchableOpacity
      onPress={() => {
        if (hasBack) {
          onBackButtonPress();
        }
      }}
    >
      {hasBack && (
        <View style={styles.iconContianer}>
          <Image source={Icons.left} style={styles.backButtonLeft} />
        </View>
      )}
    </TouchableOpacity>
    <View style={titleLeft ? styles.titleContainerLeft : styles.titleContainer}>
      <CustomText style={styles.title}>{title}</CustomText>
    </View>
    {renderRightTitle(
      hasBack,
      titleRight,
      iconRight,
      iconRightStyle,
      onRightPress
    )}
    {showAvailableStatus && renderAvailableStatus(isOnline)}
    {showFilterButton && renderFilterButton(filterButtonPress)}
  </View>
);

const renderRightTitle = (
  hasBack,
  titleRight,
  iconRight,
  iconRightStyle,
  onRightPress
) => {
  if (titleRight || iconRight) {
    return (
      <TouchableOpacity
        style={styles.rightTitleContainer}
        onPress={onRightPress}
      >
        {iconRight && (
          <Image
            source={iconRight}
            style={[styles.iconRight, iconRightStyle]}
          />
        )}
        {titleRight && (
          <CustomText style={styles.titleRight}>{titleRight}</CustomText>
        )}
      </TouchableOpacity>
    );
  }
  if (hasBack) {
    return <View style={styles.iconContianer} />;
  }
  return null;
};

const renderAvailableStatus = isOnline => {
  return (
    <View style={styles.availableStatusContainer}>
      <AvailableStatusCard isOnline={isOnline} />
    </View>
  );
};

const renderFilterButton = filterButtonPress => {
  return (
    <TouchableOpacity
      style={styles.filterButtonContainer}
      onPress={() => filterButtonPress()}
    >
      <Icon
        iconSource={Icons.filter}
        iconHeight={20}
        iconWidth={20}
        tintColor={Colors.WHITE}
      />
    </TouchableOpacity>
  );
};

export default NavigationBar;

NavigationBar.propTypes = {
  hasBack: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  navigation: PropTypes.object,
  onBackButtonPress: PropTypes.func,
  titleLeft: PropTypes.bool,
  iconRight: PropTypes.any,
  titleRight: PropTypes.string,
  iconRightStyle: PropTypes.object,
  showAvailableStatus: PropTypes.bool
};

NavigationBar.defaultProps = {
  hasBack: false,
  title: "Title",
  navigation: {},
  onBackButtonPress: () => console.log("Back button pressed, not implemented."),
  titleLeft: false,
  iconRight: false,
  titleRight: null,
  iconRightStyle: {},
  showAvailableStatus: false
};
