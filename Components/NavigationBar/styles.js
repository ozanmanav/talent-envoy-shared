import { Colors } from "@Theme";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
    height: 60,
    paddingHorizontal: 10
  },
  iconContianer: { width: 30, height: 30, justifyContent: "center" },
  iconRight: {
    width: 17,
    height: 17,
    tintColor: Colors.SUCCESS,
    marginRight: 3
  },
  titleRight: {
    color: Colors.SUCCESS,
    fontSize: 14,
    fontWeight: "500",
    marginLeft: 5
  },
  backButtonLeft: { width: 16, height: 16, tintColor: Colors.WHITE },
  titleContainer: { flex: 1, alignItems: "center" },
  titleContainerLeft: { flex: 1, alignItems: "flex-start", marginLeft: 15 },
  title: { fontSize: 18, color: "white" },
  availableStatusContainer: { flexDirection: "row", alignItems: "center" },
  rightTitleContainer: { flexDirection: "row", alignItems: "center" },
  filterButtonContainer: { flexDirection: "row", alignItems: "center" }
});
