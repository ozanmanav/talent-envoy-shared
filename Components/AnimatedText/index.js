import React from 'react'
import { Animated } from 'react-native'
import PropTypes from 'prop-types'
import { Fonts } from '@Theme'

const AnimatedText = (props) => {
  let { type, style } = props
  let fontFamily = type ? Fonts.family[type] : Fonts.family.medium

  return (
    <Animated.Text {...props} style={[style, { fontFamily }]}>
      {props.children}
    </Animated.Text>
  )
}

AnimatedText.propTypes = {
  children: PropTypes.node,
  type: PropTypes.string,
  style: PropTypes.object,
}

export default AnimatedText
