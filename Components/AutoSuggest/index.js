import React from 'react'
import { FlatList, TouchableWithoutFeedback, Text, StyleSheet } from 'react-native'
import { Colors } from '@Theme'

const keyExtractor = (item) => String(item.id)

const AutoSuggest = ({ list, onSelect }) => {
  return (
    <FlatList
      style={styles.list}
      contentContainerStyle={styles.listContainer}
      data={list}
      keyExtractor={keyExtractor}
      keyboardShouldPersistTaps="handled"
      renderItem={({ item }) => {
        return (
          <TouchableWithoutFeedback onPress={() => onSelect(item)}>
            <Text style={styles.item}>{item.name}</Text>
          </TouchableWithoutFeedback>
        )
      }}
      ListEmptyComponent={() => <Text>**No Suggestion**</Text>}
      scrollEnabled={true}
      nestedScrollEnabled={true}
    />
  )
}

export default AutoSuggest

const styles = StyleSheet.create({
  list: {
    backgroundColor: Colors.WHITE,
    borderColor: Colors.GREY_LIGHT,
    borderRadius: 5,
    borderWidth: 1,
    marginTop: 12,
    maxHeight: 200,
  },
  listContainer: {
    backgroundColor: Colors.WHITE,
    paddingHorizontal: 20,
    paddingVertical: 5,
  },
  item: {
    marginVertical: 5,
  },
})
