import React, { Component } from "react";
import { StyleSheet, Animated, Easing } from "react-native";
import { Colors } from "@Theme";

const ANIMATION_DURATION = 600;
const ANIMATION_DELAY = 100;

export default class AnimatedCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      positions: React.Children.map(
        props.children,
        x => new Animated.Value(578)
      )
    };
  }

  componentDidMount() {
    this.startAnimation();
  }

  startAnimation() {
    const animations = this.state.positions.map(x => {
      return Animated.timing(x, {
        toValue: 0,
        duration: ANIMATION_DURATION,
        easing: Easing.linear
      });
    });
    Animated.stagger(ANIMATION_DELAY, animations).start();
  }

  render() {
    const { children, cardStyle } = this.props;
    return (
      <Animated.View
        testID={this.props.testID}
        style={[
          styles.container,
          {
            transform: [{ translateY: this.state.positions[0] }]
          },
          cardStyle
        ]}
      >
        {React.Children.map(children, (child, index) => {
          let animatedContainerStyle = child.props.animatedContainerStyle;
          return (
            <Animated.View
              style={[
                animatedContainerStyle,
                { transform: [{ translateY: this.state.positions[index] }] }
              ]}
            >
              {child}
            </Animated.View>
          );
        })}
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.WHITE,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    flexGrow: 1,
    padding: 30
  }
});
