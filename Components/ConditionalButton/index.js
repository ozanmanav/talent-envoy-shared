import { Button, ConditionalLoading } from '@shared/Components';
import React, { Component } from "react";

export default class ConditionalButton extends Component {
  state = {
    result: null
  };

  componentDidUpdate(prevProps, prevState) {
    if (!prevProps.error && this.props.error) {
      this.setState({ result: false }, () => {
        setTimeout(() => {
          this.setState({ result: null });
        }, 1000);
      });
    } else if (prevProps.loading && !this.props.loading && !this.props.error) {
      this.setState({ result: true }, () => {
        setTimeout(() => {
          this.props.successAction();
        }, 1000);
      });
    }
  }

  render() {
    const { loading } = this.props;
    const { result } = this.state;
    if (loading || result === false || result === true) {
      return <ConditionalLoading isLoading={loading} result={result} />;
    }
    return <Button {...this.props} />;
  }
}
