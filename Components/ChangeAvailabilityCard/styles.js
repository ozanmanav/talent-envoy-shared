import { StyleSheet } from 'react-native'
import { Colors } from '@Theme'

const time = { fontWeight: '500', color: Colors.SECONDARY, fontSize: 14 }

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 5,
    marginHorizontal: 30,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginBottom: 20,
  },
  title: { fontWeight: '500', fontSize: 14, color: Colors.DARK, lineHeight: 18 },
  selectedTimeContainer: {
    marginRight: 15,
    borderBottomWidth: 2,
    borderColor: Colors.SECONDARY,
  },
  unselectedTimeContainer: {
    marginRight: 15,
    borderColor: Colors.DARK,
  },
  selectedTime: { ...time },
  unselectedTime: { ...time, color: Colors.DARK },
})
