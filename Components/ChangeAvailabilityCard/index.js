import { CustomText, Input } from '@shared/Components';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import styles from './styles';

export default class ChangeAvailabilityCard extends React.Component {
  state = {
    isSelectedTimeActive: false,
    selectedTimeIndex: 0,
  }

  render() {
    const { selectedTimeIndex, isSelectedTimeActive } = this.state
    const { onChange } = this.props
    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <CustomText style={styles.title}>I’m not available to take the calls</CustomText>
          <View style={{ flexDirection: 'row', marginBottom: 5 }}>
            <Time
              text="1 Hour"
              isSelected={selectedTimeIndex === 0}
              onPress={() => {
                onChange(0, isSelectedTimeActive)
                this.setState({ selectedTimeIndex: 0 })
              }}
            />
            <Time
              text="1 Day"
              isSelected={selectedTimeIndex === 1}
              onPress={() => {
                onChange(1, isSelectedTimeActive)
                this.setState({ selectedTimeIndex: 1 })
              }}
            />
          </View>
        </View>
        <View style={{ flex: 0.25 }}>
          <Input.Switch
            selected={isSelectedTimeActive}
            label=""
            onChange={(isSelected) => {
              onChange(selectedTimeIndex, isSelected)
              this.setState({ isSelectedTimeActive: !isSelectedTimeActive })
            }}
          />
        </View>
      </View>
    )
  }
}

const Time = ({ isSelected, text, onPress }) => {
  const containerStyle = isSelected ? styles.selectedTimeContainer : styles.unselectedTimeContainer
  const timeStyle = isSelected ? styles.selectedTime : styles.unselectedTime
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={containerStyle}>
        <CustomText style={timeStyle}>{text}</CustomText>
      </View>
    </TouchableOpacity>
  )
}
