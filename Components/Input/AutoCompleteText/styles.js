import { StyleSheet } from "react-native";
import theme from "../theme";

export const getContainerStyle = (hasError, isFocused, value) => ({
  height: theme.CONTAINER.height,
  borderWidth: theme.CONTAINER.borderWidth,
  borderRadius: theme.CONTAINER.borderRadius,
  borderColor: hasError
    ? theme.CONTAINER.borderColor._ERROR
    : isFocused
    ? theme.CONTAINER.borderColor._FOCUSED
    : theme.CONTAINER.borderColor._OTHER,
  backgroundColor: hasError
    ? theme.CONTAINER.backgroundColor._ERROR
    : isFocused || Boolean(value)
    ? theme.CONTAINER.backgroundColor._FOCUSED_OR_FILLED
    : theme.CONTAINER.backgroundColor._OTHER
});

export const getLabelStyle = (anim, deactive) => ({
  fontFamily: "CircularStd-Medium",
  position: "absolute",
  left: theme.CONTAINER.paddingLeft,
  bottom: anim.interpolate({
    inputRange: [0, 1],
    outputRange: [theme.LABEL.bottom._BIG, theme.LABEL.bottom._SMALL]
  }),
  fontSize: anim.interpolate({
    inputRange: [0, 1],
    outputRange: [theme.LABEL.fontSize._BIG, theme.LABEL.fontSize._SMALL]
  }),
  color: theme.LABEL.color,
  opacity: deactive
    ? theme.LABEL.opacity._DEACTIVE
    : theme.LABEL.opacity._NORMAL
});

export const getInputStyle = preText => ({
  height: theme.CONTAINER.height,

  paddingLeft:
    theme.CONTAINER.paddingLeft + (preText ? preText.length * 7 + 4 : 0),
  paddingTop: theme.INPUT.paddingTop
});

export const styles = StyleSheet.create({
  // input: {
  //   height: theme.CONTAINER.height,
  //   paddingLeft: theme.CONTAINER.paddingLeft,
  //   paddingTop: theme.INPUT.paddingTop,
  // },
  preText: {
    color: theme.LABEL.color,
    left: theme.CONTAINER.paddingLeft,
    lineHeight: 20,
    position: "absolute"
  }
});
