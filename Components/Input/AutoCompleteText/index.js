import React from "react";
import { Animated, View } from "react-native";
import { CustomText, AnimatedText, InputAutoSuggest } from "../index";
import PropTypes from "prop-types";
import { compose, withStateHandlers, lifecycle } from "recompose";
import {
  getContainerStyle,
  getLabelStyle,
  styles,
  getInputStyle
} from "./styles";
import ItSkillsWithId from "./it_skills_with_id.json";
const AutoCompleteText = ({
  isFocused,
  hasError,
  deactive,
  label,
  preText,
  value,
  onChangeText,
  onFocus,
  onBlur,
  autoCapitalize,
  keyboardType,
  blurOnSubmit,
  onSubmitEditing,
  anim,
  setInputRef
}) => {
  const containerStyle = getContainerStyle(hasError, isFocused, value);
  const labelStyle = getLabelStyle(anim, deactive);
  const inputStyle = getInputStyle(preText);

  return (
    <>
      <View style={containerStyle}>
        <AnimatedText style={labelStyle}>{label}</AnimatedText>
        <InputAutoSuggest
          setInputRef={setInputRef}
          style={inputStyle}
          blurOnSubmit={blurOnSubmit}
          onFocus={onFocus}
          onBlur={onBlur}
          staticData={ItSkillsWithId}
          onChangeText={onChangeText}
          onSubmitEditing={onSubmitEditing}
          autoCapitalize={autoCapitalize}
        />

        {preText && (isFocused || value) ? (
          <CustomText type="book" style={styles.preText}>
            {preText}
          </CustomText>
        ) : null}
      </View>
    </>
  );
};

export default compose(
  withStateHandlers(
    props => ({
      anim: new Animated.Value(props.value ? 1 : 0),
      inputRef: null
    }),
    {
      setInputRef: () => inputRef => ({ inputRef })
    }
  ),
  lifecycle({
    componentDidUpdate(prevProps) {
      const prevTo = prevProps.isFocused || prevProps.value !== "";
      const currentTo = this.props.isFocused || this.props.value !== "";

      if (prevTo !== currentTo) {
        Animated.timing(this.props.anim, {
          toValue: this.props.isFocused || this.props.value !== "" ? 1 : 0,
          duration: 200
        }).start();

        if (!currentTo && this.props.inputRef) this.props.inputRef.blur();
      }
    }
  })
)(AutoCompleteText);

AutoCompleteText.propTypes = {
  isFocused: PropTypes.bool.isRequired,
  hasError: PropTypes.bool.isRequired,
  deactive: PropTypes.bool.isRequired,
  label: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  autoCapitalize: PropTypes.string.isRequired,
  keyboardType: PropTypes.string.isRequired,
  blurOnSubmit: PropTypes.bool.isRequired,
  onSubmitEditing: PropTypes.func,
  anim: PropTypes.instanceOf(Animated.Value).isRequired,
  setInputRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(AutoCompleteText) })
  ]),
  preText: PropTypes.string
};

AutoCompleteText.defaultProps = {
  isFocused: false,
  hasError: false,
  deactive: false,
  value: "",
  onChangeText: () => {},
  onFocus: () => {},
  onBlur: () => {},
  autoCapitalize: "sentences",
  keyboardType: "default",
  blurOnSubmit: true
};
