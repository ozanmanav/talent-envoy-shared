import { StyleSheet } from "react-native";
import theme from "../theme";

export const getContainerStyle = (hasError, isFocused, value) => ({
  height: 250,
  borderWidth: theme.CONTAINER.borderWidth,
  borderRadius: theme.CONTAINER.borderRadius,
  borderColor: hasError
    ? theme.CONTAINER.borderColor._ERROR
    : isFocused
    ? theme.CONTAINER.borderColor._FOCUSED
    : theme.CONTAINER.borderColor._OTHER,
  backgroundColor: hasError
    ? theme.CONTAINER.backgroundColor._ERROR
    : isFocused || Boolean(value)
    ? theme.CONTAINER.backgroundColor._FOCUSED_OR_FILLED
    : theme.CONTAINER.backgroundColor._OTHER
});

export const getLabelStyle = (anim, deactive) => ({
  fontFamily: "CircularStd-Medium",
  position: "absolute",
  left: theme.CONTAINER.paddingLeft,
  top: anim.interpolate({
    inputRange: [0, 1],
    outputRange: [16, 7]
  }),
  fontSize: anim.interpolate({
    inputRange: [0, 1],
    outputRange: [theme.LABEL.fontSize._BIG, theme.LABEL.fontSize._SMALL]
  }),
  color: theme.LABEL.color,
  opacity: deactive
    ? theme.LABEL.opacity._DEACTIVE
    : theme.LABEL.opacity._NORMAL
});

export const styles = StyleSheet.create({
  input: {
    height: 225,
    paddingLeft: theme.CONTAINER.paddingLeft,
    paddingRight: theme.CONTAINER.paddingLeft,
    marginTop: 22
  },
  preText: {
    color: theme.LABEL.color,
    left: theme.CONTAINER.paddingLeft,
    lineHeight: 20,
    position: "absolute",
    top: theme.CONTAINER.height / 2 - 6
  }
});
