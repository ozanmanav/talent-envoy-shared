import { CustomText } from '@shared/Components';
import React from 'react';
import { StyleSheet, Switch as SwitchRN, View } from 'react-native';
import { compose, withHandlers } from 'recompose';
import theme from '../theme';

const Switch = ({ selected, label, onChangeHandler, deactive }) => {
  return (
    <View style={styles.container}>
      <CustomText type="book" style={[styles.label]}>
        {label}
      </CustomText>
      <SwitchRN
        style={styles.switch}
        value={selected}
        onValueChange={onChangeHandler}
        disabled={deactive}
      />
    </View>
  )
}

export default compose(
  withHandlers({
    onChangeHandler: ({ onChange, selected, name }) => () => onChange(!selected, name),
  })
)(Switch)

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: theme.CHECKBOX_CONTAINER.marginVertical,
  },
  label: {
    color: theme.CHECKBOX_LABEL.color,
    marginLeft: theme.CHECK_LABEL_LEFT_MARGIN,
  },
  switch: {
    transform: theme.SWITCH.transform,
  },
})
