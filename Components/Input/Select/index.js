import { AnimatedText, CustomText } from "@shared/Components";
import PropTypes from "prop-types";
import React from "react";
import { Animated, Keyboard, StyleSheet, TouchableWithoutFeedback, View } from "react-native";
import { compose, lifecycle, withHandlers, withStateHandlers } from "recompose";
import theme from "../theme";


const Select = ({
  isFocused,
  hasError,
  deactive,
  label,
  value,
  onFocusHandler,
  anim,
  containerCustomStyle
}) => {
  const containerStyle = getContainerStyle(
    hasError,
    isFocused,
    value,
    containerCustomStyle
  );
  const labelStyle = getLabelStyle(anim, deactive);

  console.log("TalentFilterModalIndex value", value);

  return (
    <TouchableWithoutFeedback onPress={onFocusHandler}>
      <View style={containerStyle}>
        <AnimatedText style={labelStyle}>{label}</AnimatedText>
        <CustomText style={styles.text}>{value}</CustomText>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default compose(
  withStateHandlers(
    props => ({
      anim: new Animated.Value(props.value ? 1 : 0)
    }),
    {}
  ),
  lifecycle({
    componentDidUpdate(prevProps) {
      const prevTo = prevProps.isFocused || prevProps.value !== "";
      const currentTo = this.props.isFocused || this.props.value !== "";

      if (prevTo !== currentTo) {
        Animated.timing(this.props.anim, {
          toValue: this.props.isFocused || this.props.value !== "" ? 1 : 0,
          duration: 200
        }).start();
      }
    }
  }),
  withHandlers({
    onFocusHandler: ({ onFocus }) => () => {
      Keyboard.dismiss();
      onFocus();
    }
  })
)(Select);

Select.propTypes = {
  isFocused: PropTypes.bool.isRequired,
  hasError: PropTypes.bool.isRequired,
  deactive: PropTypes.bool.isRequired,
  label: PropTypes.string,
  value: PropTypes.string.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  anim: PropTypes.instanceOf(Animated.Value).isRequired,
  onFocusHandler: PropTypes.func,
  children: PropTypes.node
};

Select.defaultProps = {
  isFocused: false,
  hasError: false,
  deactive: false,
  // label,
  value: "",
  onFocus: () => {},
  onBlur: () => {}
  // anim,
};

const getContainerStyle = (hasError, isFocused, value, containerStyle) => ({
  height: theme.CONTAINER.height,
  borderWidth: theme.CONTAINER.borderWidth,
  borderRadius: theme.CONTAINER.borderRadius,
  borderColor: hasError
    ? theme.CONTAINER.borderColor._ERROR
    : isFocused
    ? theme.CONTAINER.borderColor._FOCUSED
    : theme.CONTAINER.borderColor._OTHER,
  backgroundColor: hasError
    ? theme.CONTAINER.backgroundColor._ERROR
    : isFocused || Boolean(value)
    ? theme.CONTAINER.backgroundColor._FOCUSED_OR_FILLED
    : theme.CONTAINER.backgroundColor._OTHER,
  ...containerStyle
});

const getLabelStyle = (anim, deactive) => ({
  fontFamily: "CircularStd-Black",
  position: "absolute",
  left: theme.CONTAINER.paddingLeft,
  bottom: anim.interpolate({
    inputRange: [0, 1],
    outputRange: [theme.LABEL.bottom._BIG, theme.LABEL.bottom._SMALL]
  }),
  fontSize: anim.interpolate({
    inputRange: [0, 1],
    outputRange: [theme.LABEL.fontSize._BIG, theme.LABEL.fontSize._SMALL]
  }),
  color: theme.LABEL.color,
  opacity: deactive
    ? theme.LABEL.opacity._DEACTIVE
    : theme.LABEL.opacity._NORMAL
});

const styles = StyleSheet.create({
  text: {
    height: theme.CONTAINER.height,
    paddingLeft: theme.CONTAINER.paddingLeft,
    paddingTop: theme.SELECT_INPUT.paddingTop
  }
});
