import { CustomText } from '@shared/Components';
import { Colors } from '@Theme';
import React, { Component } from 'react';
import { StyleSheet, Switch as SwitchRN, View } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import theme from '../theme';

class SwitchCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isSelected: props.selected,
    }
    this.onValueChange = this.onValueChange.bind(this)
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.selected !== prevState.isSelected) {
      return {
        isSelected: nextProps.selected,
      }
    }
    return null
  }

  onValueChange = (value) => {
    const { onChange } = this.props
    onChange(value)
    this.setState({ isSelected: value })
  }

  render() {
    const { isSelected } = this.state
    const { selectedText, unselectedText, selectedTimes } = this.props

    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <CustomText type="book" style={styles.label}>
            {isSelected ? selectedText : unselectedText}
          </CustomText>
          {selectedTimes &&
            selectedTimes.length > 0 &&
            selectedTimes.map((time, index) => {
              return (
                <CustomText key={index} type="book" style={styles.label}>
                  {time}
                </CustomText>
              )
            })}
        </View>
        <SwitchRN style={styles.switch} value={isSelected} onValueChange={this.onValueChange} />
      </View>
    )
  }
}

export default SwitchCard

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: theme.CHECKBOX_CONTAINER.marginVertical,
    paddingVertical: wp('2.5'),
    paddingHorizontal: wp('5'),
    borderColor: Colors.GREY_LIGHTEST,
    borderWidth: 1,
    borderRadius: 5,
  },
  label: {
    color: theme.CHECKBOX_LABEL.color,
    marginLeft: theme.CHECK_LABEL_LEFT_MARGIN,
    fontSize: 14,
  },
  switch: {
    // transform: theme.SWITCH.transform,
  },
})
