import { CustomText } from '@shared/Components';
import { Colors } from "@Theme";
import React, { Component } from "react";
import { StyleSheet, Switch as SwitchRN, View } from "react-native";
import theme from "../theme";

class SwitchCard extends Component {
  render() {
    const { name, selected, onChange, title, lines } = this.props;

    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <CustomText type="book" style={styles.label}>
            {title}
          </CustomText>
          {lines &&
            lines.map((line, index) => {
              return (
                <CustomText key={index} type="book" style={styles.label}>
                  {line}
                </CustomText>
              );
            })}
        </View>
        <SwitchRN
          style={styles.switch}
          value={selected}
          onValueChange={value => onChange(value, name)}
        />
      </View>
    );
  }
}

export default SwitchCard;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
    marginVertical: theme.CHECKBOX_CONTAINER.marginVertical,
    // paddingVertical: wp('2.5'),
    // paddingHorizontal: wp('5'),
    borderColor: Colors.GREY_LIGHTEST,
    borderWidth: 1,
    borderRadius: 5
  },
  label: {
    color: theme.CHECKBOX_LABEL.color,
    marginLeft: theme.CHECK_LABEL_LEFT_MARGIN,
    fontSize: 14
  },
  switch: {
    // transform: theme.SWITCH.transform,
  }
});
