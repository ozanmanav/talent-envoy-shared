import { CustomText } from '@shared/Components';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import theme from '../theme';

const PlainText = ({ selected, label, value, onChangeHandler, hidden, preText }) => {
  if (hidden) {
    return null
  }

  return (
    <View style={styles.container}>
      {label && (
        <CustomText type="book" style={[styles.label]}>
          {label}
        </CustomText>
      )}
      {value && (
        <CustomText type="book" style={[styles.label]}>
          {preText} {value}
        </CustomText>
      )}
    </View>
  )
}

export default PlainText

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: theme.CHECKBOX_CONTAINER.marginVertical,
  },
  label: {
    color: theme.CHECKBOX_LABEL.color,
    marginLeft: theme.CHECK_LABEL_LEFT_MARGIN,
  },
})
