import { WithFieldProps, WithOnCheckChange, WithOnRadioSelected, WithTitle } from "@shared/Components";
import { compose } from "recompose";
import AutoCompleteText from "./AutoCompleteText";
import Checkbox from "./Checkbox";
import Password from "./Password";
import PlainText from "./PlainText";
import Radio from "./Radio";
import Select from "./Select";
import Switch from "./Switch";
import SwitchCard from "./SwitchCard";
import Text from "./Text";
import TextArea from "./TextArea";

const enhance = compose(
  WithTitle,
  WithFieldProps
);

export default {
  Text: enhance(Text),
  AutoCompleteText: enhance(AutoCompleteText),
  Password: enhance(Password),
  Checkbox: WithOnCheckChange(Checkbox),
  Radio: WithOnRadioSelected(Radio),
  Switch: WithOnCheckChange(Switch),
  SwitchCard,
  Select,
  PlainText,
  TextArea: enhance(TextArea)
};
