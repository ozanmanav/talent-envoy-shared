import { StyleSheet } from "react-native";
import theme from "../theme";

const commonCheck = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: theme.CHECKBOX.backgroundColor,
  borderWidth: theme.CHECKBOX.borderWidth,
  borderRadius: theme.CHECKBOX.borderRadius,
  width: theme.CHECKBOX.height,
  height: theme.CHECKBOX.height,
  marginRight: theme.CHECKBOX.marginRight
};
const commonIcon = {
  width: theme.CHECKBOX_ICON.width,
  height: theme.CHECKBOX_ICON.height
};

export default StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    marginVertical: theme.CHECKBOX_CONTAINER.marginVertical
  },
  selected: {
    ...commonCheck,
    borderColor: theme.CHECKBOX.borderColor._SELECTED
  },
  notSelected: {
    ...commonCheck,
    borderColor: theme.CHECKBOX.borderColor._NOT_SELECTED
  },
  iconSelected: {
    ...commonIcon,
    tintColor: theme.CHECKBOX_ICON.tintColor._SELECTED
  },
  iconNotSelected: {
    ...commonIcon,
    tintColor: theme.CHECKBOX_ICON.tintColor._NOT_SELECTED
  },

  label: {
    marginLeft: theme.CHECK_LABEL_LEFT_MARGIN,
    color: theme.CHECKBOX_LABEL.color
  },
  label2: {
    marginLeft: theme.CHECK_LABEL_LEFT_MARGIN,
    marginTop: theme.CHECK_LABEL_TOP_MARGIN.marginTop,
    color: theme.CHECKBOX_DESC_LABEL.color
  }
});
