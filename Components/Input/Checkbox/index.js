import { CustomText } from "@shared/Components";
import { Colors, Icons } from "@Theme";
import React from "react";
import { Image, TouchableWithoutFeedback, View } from "react-native";
import { compose, withHandlers } from "recompose";
import styles from "./styles";

const Check = ({
  selected,
  label,
  onChangeHandler,
  deactive,
  isMultilineLabel,
  label2
}) => {
  const style = selected ? styles.selected : styles.notSelected;
  const iconStyle = selected ? styles.iconSelected : styles.iconNotSelected;

  return (
    <TouchableWithoutFeedback onPress={onChangeHandler} disabled={deactive}>
      <View style={[styles.container]}>
        <View style={[style, deactive && { borderColor: Colors.GREY }]}>
          <Image
            style={[iconStyle, deactive && { tintColor: Colors.GREY }]}
            source={Icons.success}
          />
        </View>
        {!isMultilineLabel && (
          <CustomText type="book" style={styles.label}>
            {label}
          </CustomText>
        )}

        {isMultilineLabel && (
          <View>
            <CustomText type="book" style={styles.label}>
              {label}
            </CustomText>
            <CustomText type="book" style={styles.label2}>
              {label2}
            </CustomText>
          </View>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default compose(
  withHandlers({
    onChangeHandler: ({ onChange, selected, name }) => () =>
      onChange(!selected, name)
  })
)(Check);
