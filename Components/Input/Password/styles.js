import { StyleSheet } from "react-native";
import theme from "../theme";

const SECURE_DIMENSION = theme.CONTAINER.height * 0.4;

export const getContainerStyle = (hasError, isFocused, value) => ({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  height: theme.CONTAINER.height,
  borderWidth: theme.CONTAINER.borderWidth,
  borderRadius: theme.CONTAINER.borderRadius,
  borderColor: hasError
    ? theme.CONTAINER.borderColor._ERROR
    : isFocused
    ? theme.CONTAINER.borderColor._FOCUSED
    : theme.CONTAINER.borderColor._OTHER,
  backgroundColor: hasError
    ? theme.CONTAINER.backgroundColor._ERROR
    : isFocused || Boolean(value)
    ? theme.CONTAINER.backgroundColor._FOCUSED_OR_FILLED
    : theme.CONTAINER.backgroundColor._OTHER
});

export const getLabelStyle = anim => {
  return {
    position: "absolute",
    left: theme.CONTAINER.paddingLeft,
    bottom: anim.interpolate({
      inputRange: [0, 1],
      outputRange: [theme.LABEL.bottom._BIG, theme.LABEL.bottom._SMALL]
    }),
    fontSize: anim.interpolate({
      inputRange: [0, 1],
      outputRange: [theme.LABEL.fontSize._BIG, theme.LABEL.fontSize._SMALL]
    }),
    color: theme.LABEL.color
  };
};

export const styles = StyleSheet.create({
  input: {
    flex: 1,
    height: theme.CONTAINER.height,
    paddingLeft: theme.CONTAINER.paddingLeft,
    paddingTop: theme.INPUT.paddingTop
  },
  secure: {
    height: SECURE_DIMENSION,
    width: SECURE_DIMENSION
  },
  secureContainer: {
    marginRight: SECURE_DIMENSION,
    padding: 3
  }
});
