import { AnimatedText } from '@shared/Components';
import { Icons } from '@Theme';
import PropTypes from 'prop-types';
import React from 'react';
import { Animated, Image, TextInput, TouchableOpacity, View } from 'react-native';
import { compose, lifecycle, withStateHandlers } from 'recompose';
import { getContainerStyle, getLabelStyle, styles } from './styles';

const Password = ({
  isFocused,
  hasError,
  deactive,
  label,
  value,
  onChangeText,
  onFocus,
  onBlur,
  anim,
  secureTextEntry,
  toggleSecureTextEntry,
  setInputRef,
}) => {
  const containerStyle = getContainerStyle(hasError, isFocused, value)
  const labelStyle = getLabelStyle(anim)

  const secureSource = Icons[secureTextEntry ? 'eyeOff' : 'eyeOn']

  return (
    <View style={containerStyle}>
      <AnimatedText style={labelStyle}>{label}</AnimatedText>
      <TextInput
        style={styles.input}
        ref={setInputRef}
        autoCapitalize="none"
        keyboardType="default"
        editable={!deactive}
        secureTextEntry={secureTextEntry}
        value={value}
        onChangeText={onChangeText}
        onFocus={onFocus}
        onBlur={onBlur}
      />
      <TouchableOpacity style={styles.secureContainer} onPress={toggleSecureTextEntry}>
        <Image style={styles.secure} source={secureSource} resizeMode="contain" />
      </TouchableOpacity>
    </View>
  )
}

export default compose(
  withStateHandlers(
    (props) => ({
      anim: new Animated.Value(props.value ? 1 : 0),
      inputRef: null,
      secureTextEntry: true,
    }),
    {
      setInputRef: () => (inputRef) => ({ inputRef }),
      toggleSecureTextEntry: ({ secureTextEntry }) => () => ({ secureTextEntry: !secureTextEntry }),
    }
  ),
  lifecycle({
    componentDidUpdate(prevProps) {
      const prevTo = prevProps.isFocused || prevProps.value !== ''
      const currentTo = this.props.isFocused || this.props.value !== ''

      if (prevTo !== currentTo) {
        Animated.timing(this.props.anim, {
          toValue: this.props.isFocused || this.props.value !== '' ? 1 : 0,
          duration: 200,
        }).start()

        if (!currentTo) this.props.inputRef.blur()
      }
    },
  })
)(Password)

Password.propTypes = {
  isFocused: PropTypes.bool.isRequired,
  hasError: PropTypes.bool.isRequired,
  deactive: PropTypes.bool.isRequired,
  label: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  autoCapitalize: PropTypes.string.isRequired,
  keyboardType: PropTypes.string.isRequired,
  anim: PropTypes.instanceOf(Animated.Value).isRequired,
  secureTextEntry: PropTypes.bool.isRequired,
  toggleSecureTextEntry: PropTypes.func.isRequired,
  setInputRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Password) }),
  ]),
}

Password.defaultProps = {
  isFocused: false,
  hasError: false,
  deactive: false,
  // label,
  value: '',
  onChangeText: () => {},
  onFocus: () => {},
  onBlur: () => {},
  autoCapitalize: 'none',
  keyboardType: 'default',
  // anim,
  // secureTextEntry,
  // toogleSecureTextEntry
}
