import { Platform } from "react-native";
import { Colors } from "@Theme";

export default {
  CONTAINER: {
    height: 50,
    paddingLeft: 20,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: {
      _ERROR: Colors.ERROR,
      _FOCUSED: Colors.DARK,
      _OTHER: Colors.GREY_LIGHT
    },
    backgroundColor: {
      _ERROR: Colors.ERROR_LIGHT,
      _FOCUSED_OR_FILLED: Colors.WHITE,
      _OTHER: Colors.GREY_LIGHTEST
    }
  },
  LABEL: {
    position: "absolute",
    color: Colors.DARK,
    bottom: {
      _SMALL: 28,
      _BIG: 16
    },
    fontSize: {
      _SMALL: 10,
      _BIG: 14
    },
    opacity: {
      _DEACTIVE: 0.5,
      _NORMAL: 1
    }
  },
  INPUT: {
    paddingTop: Platform.OS === "ios" ? 10 : 20
  },

  SELECT_INPUT: {
    paddingTop: 20
  },

  CHECKBOX_CONTAINER: {
    marginVertical: 4
  },
  CHECKBOX: {
    height: 22,
    borderWidth: 2,
    borderRadius: 4,
    backgroundColor: Colors.WHITE,
    marginRight: 8,
    borderColor: {
      _SELECTED: Colors.SUCCESS,
      _NOT_SELECTED: Colors.SECONDARY
    }
  },
  CHECKBOX_ICON: {
    height: 10,
    width: 13,
    tintColor: {
      _SELECTED: Colors.SUCCESS,
      _NOT_SELECTED: Colors.WHITE
    }
  },
  RADIO_ICON: {
    height: 9,
    width: 11,
    tintColor: {
      _SELECTED: Colors.SUCCESS,
      _NOT_SELECTED: Colors.WHITE
    }
  },
  CHECKBOX_LABEL: {
    color: Colors.DARK
  },
  CHECKBOX_DESC_LABEL: {
    color: Colors.DARK_GREY
  },
  CHECK_LABEL_TOP_MARGIN: {
    marginTop: 5
  },

  SWITCH: {
    transform: [{ scaleX: 0.85 }, { scaleY: 0.85 }]
  }
};
