import { AnimatedText, CustomText } from "@shared/Components";
import PropTypes from "prop-types";
import React from "react";
import { Animated, TextInput, View } from "react-native";
import { compose, lifecycle, withStateHandlers } from "recompose";
import { getContainerStyle, getInputStyle, getLabelStyle, styles } from "./styles";


const Text = ({
  isFocused,
  hasError,
  deactive,
  label,
  preText,
  value,
  onChangeText,
  onFocus,
  onBlur,
  autoCapitalize,
  keyboardType,
  blurOnSubmit,
  onSubmitEditing,
  anim,
  setInputRef,
  placeholder
}) => {
  const containerStyle = getContainerStyle(hasError, isFocused, value);
  const labelStyle = getLabelStyle(anim, deactive);
  const inputStyle = getInputStyle(preText);

  console.log("value", value);

  return (
    <>
      <View style={containerStyle}>
        <AnimatedText style={labelStyle}> {label} </AnimatedText>
        <TextInput
          // style={styles.input}
          style={inputStyle}
          ref={setInputRef}
          autoCapitalize={autoCapitalize}
          keyboardType={keyboardType}
          blurOnSubmit={blurOnSubmit}
          onSubmitEditing={onSubmitEditing}
          editable={!deactive}
          value={value}
          onChangeText={onChangeText}
          onFocus={onFocus}
          onBlur={onBlur}
          placeholder={placeholder}
        />
        {preText && (isFocused || value) ? (
          <CustomText type="book" style={styles.preText}>
            {preText}
          </CustomText>
        ) : null}
      </View>
    </>
  );
};

export default compose(
  withStateHandlers(
    props => ({
      anim: new Animated.Value(props.value ? 1 : 0),
      inputRef: null
    }),
    {
      setInputRef: () => inputRef => ({ inputRef })
    }
  ),
  lifecycle({
    componentDidUpdate(prevProps) {
      const prevTo = prevProps.isFocused || prevProps.value !== "";
      const currentTo = this.props.isFocused || this.props.value !== "";

      if (prevTo !== currentTo) {
        Animated.timing(this.props.anim, {
          toValue: this.props.isFocused || this.props.value !== "" ? 1 : 0,
          duration: 200
        }).start();

        if (!currentTo) this.props.inputRef.blur();
      }
      // if (prevProps.isFocused && !this.props.isFocused) this.props.inputRef.blur()
    }
  })
)(Text);

Text.propTypes = {
  isFocused: PropTypes.bool.isRequired,
  hasError: PropTypes.bool.isRequired,
  deactive: PropTypes.bool.isRequired,
  label: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  autoCapitalize: PropTypes.string.isRequired,
  keyboardType: PropTypes.string.isRequired,
  blurOnSubmit: PropTypes.bool.isRequired,
  onSubmitEditing: PropTypes.func,
  anim: PropTypes.instanceOf(Animated.Value).isRequired,
  setInputRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Text) })
  ]),
  preText: PropTypes.string
};

Text.defaultProps = {
  isFocused: false,
  hasError: false,
  deactive: false,
  // label,
  value: "",
  onChangeText: () => {},
  onFocus: () => {},
  onBlur: () => {},
  autoCapitalize: "sentences",
  keyboardType: "default",
  blurOnSubmit: true
  // anim,
};
