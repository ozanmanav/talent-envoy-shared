import { CustomText } from '@shared/Components';
import { Icons } from '@Theme';
import React from 'react';
import { Image, TouchableWithoutFeedback, View } from 'react-native';
import { compose, withHandlers } from 'recompose';
import styles from './styles';

const Radio = ({ selected, label, onSelectedHandler, deactive }) => {
  const style = selected ? styles.selected : styles.notSelected
  const iconStyle = selected ? styles.iconSelected : styles.iconNotSelected

  return (
    <TouchableWithoutFeedback onPress={onSelectedHandler} disabled={deactive}>
      <View style={styles.container}>
        <View style={style}>
          <Image style={iconStyle} source={Icons.check} />
        </View>
        <CustomText type="book" style={styles.label}>
          {label}
        </CustomText>
      </View>
    </TouchableWithoutFeedback>
  )
}

export default compose(
  withHandlers({
    onSelectedHandler: ({ onSelected, selected, val }) => () => !selected && onSelected(val),
  })
)(Radio)
