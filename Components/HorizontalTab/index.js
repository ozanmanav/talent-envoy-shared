import Container from "./Container";
import withHorizontalTabHandlers from "./withHorizontalTabHandlers";

export default {
  Container,
  withHorizontalTabHandlers
};
