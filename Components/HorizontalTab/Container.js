import React from "react";
import { FlatList, Text, View, ScrollView, StyleSheet } from "react-native";
import Tab from "./Tab";

const keyExtractor = item => item.id;

const HorizontalTabSliderIndex = ({
  tabsData,
  selectedTab,
  changeSelectedTab
}) => {
  return (
    <View style={styles.container}>
      <FlatList
        style={styles.flatList}
        contentContainerStyle={styles.flatListContent}
        horizontal
        showsHorizontalScrollIndicator={false}
        bounces={false}
        keyExtractor={keyExtractor}
        extraData={selectedTab}
        data={tabsData}
        renderItem={({ item }) => {
          const selected = item.id === selectedTab;
          return (
            <Tab item={item} onPress={changeSelectedTab} selected={selected} />
          );
        }}
      />
    </View>
  );
};

export default HorizontalTabSliderIndex;

const styles = StyleSheet.create({
  container: {
    display: "flex",
    alignItems: "stretch"
  },
  flatList: {
    height: 50
  },
  flatListContent: {
    paddingLeft: 10
  }
});
