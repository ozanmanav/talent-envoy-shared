import { CustomText, Icon } from "@shared/Components";
import { Colors } from "@Theme";
import React from "react";
import { StyleSheet, TouchableWithoutFeedback, View } from "react-native";
import { withHandlers } from "recompose";

const Tab = ({ item, selected, onPressHandler }) => {
  const containerStyleWithPadding = selected
    ? styles.selectedContainer
    : styles.normalContainer;
  const containerStyle = item.icon
    ? containerStyleWithPadding
    : { ...containerStyleWithPadding, paddingLeft: 0 };
  const textStyle = selected ? styles.selectedText : styles.normalText;
  const tintColor = selected ? Colors.WHITE : Colors.DARK;

  return (
    <TouchableWithoutFeedback onPress={onPressHandler}>
      <View style={containerStyle}>
        {item.icon && (
          <Icon
            iconSource={item.icon}
            iconHeight={16}
            iconWidth={16}
            tintColor={tintColor}
          />
        )}
        <CustomText style={textStyle}>{item.title}</CustomText>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default withHandlers({
  onPressHandler: ({ item, onPress }) => () => onPress(item.id)
})(Tab);

const container = {
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  height: 30,
  borderRadius: 5,
  marginHorizontal: 5,
  paddingLeft: 12
};
const text = {
  marginHorizontal: 10
};
const styles = StyleSheet.create({
  normalContainer: {
    ...container,
    backgroundColor: Colors.WHITE
  },
  selectedContainer: {
    ...container,
    backgroundColor: Colors.PRIMARY
  },
  normalText: {
    ...text,
    color: Colors.DARK
  },
  selectedText: {
    ...text,
    color: Colors.WHITE
  }
});
