import { compose, mapProps, withStateHandlers } from "recompose";
// import tabsData from './tabs'
// import tabsData from '@Screens/OnTheBatch/BatchHome/tabs'

export default compose(
  withStateHandlers(
    { selectedTab: "all" },
    {
      changeSelectedTab: () => selectedTab => ({ selectedTab })
    }
  ),
  mapProps(({ tabsData, selectedTab, changeSelectedTab, ...props }) => ({
    tabs: { tabsData, selectedTab, changeSelectedTab },
    ...props
  }))
);
