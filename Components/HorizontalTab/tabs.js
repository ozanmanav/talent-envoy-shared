import { Icons } from "@Theme";

export default [
  { id: "all", title: "All Calls", icon: Icons.call },
  { id: "scheduled", title: "Scheduled Calls", icon: Icons.scheduledCall },
  { id: "missed", title: "Missed Calls", icon: Icons.missedCall },
  { id: "incoming", title: "Incoming Calls", icon: Icons.missedCall },
  { id: "outgoing", title: "Outgoing Calls", icon: Icons.missedCall }
];
