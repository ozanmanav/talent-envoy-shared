import React from "react";
import { Text, View, Slider as SliderRN, StyleSheet } from "react-native";
import WithTitle from "../Hocs/withTitle";
import { compose, withStateHandlers } from "recompose";
import { Colors } from "@Theme";

const Slider = ({
  initialValue,
  minValue,
  maxValue,
  currentValue,
  _onChange
}) => {
  return (
    <>
      <SliderRN
        value={initialValue}
        onValueChange={_onChange}
        maximumValue={maxValue}
        minimumValue={minValue}
        step={1}
        style={styles.slider}
        maximumTrackTintColor="#f5f5f5"
        minimumTrackTintColor="#f5f5f5"
        // thumbTintColor="#00D157"
        // thumbImage={Icons.check}
      />
      <View style={styles.texts}>
        <Text style={styles.minValue}>{minValue}</Text>
        <Text style={styles.currentValue}>{currentValue}</Text>
        <Text style={styles.maxValue}>{maxValue}+</Text>
      </View>
    </>
  );
};

export default compose(
  WithTitle,
  withStateHandlers(({ initialValue }) => ({ currentValue: initialValue }), {
    _onChange: (state, { onChange }) => currentValue => {
      onChange(currentValue);
      return { currentValue };
    }
  })
)(Slider);

const styles = StyleSheet.create({
  slider: {
    height: 30,
    backgroundColor: Colors.GREY_LIGHTEST,
    borderColor: Colors.GREY_LIGHT,
    borderWidth: 1,
    borderRadius: 15
  },
  texts: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 8
  },
  minValue: {
    color: Colors.DARK
  },
  maxValue: {
    color: Colors.DARK
  },
  currentValue: {
    color: Colors.SUCCESS
  }
});
